# lab3

вариант №3:
Тип коллекции | типы хранимых элементов | дополнительные операции
--------------|-------------------------|-----------------------------------------
Стек          | Целые числа             | map, where, reduce
.             | Вещественные числа      | Конкатенация
.             | Комплексные числа       | Извлечение подпоследовательности
.             | Строки/символы          | Поиск на вхождение подпоследовательности
.             | Функции
.             | Студенты
.             | Преподаватели

## How to build project

```
git clone https://gitlab.com/Roma004/labs_mephi_sem_2.git
cd lab3
mkdir build
cd build
cmake ..
make
```

## How to run project

To run cli test interface: `/lab3/build/cli`
To run cli test interface: `/lab3/build/test`
