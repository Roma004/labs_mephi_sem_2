#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <ostream>
#include <ratio>
#include <sstream>
#include <stdexcept>
#include <string>
#include <iostream>
#include <chrono>

#include "include/Stack.hpp"
#include "../lab2/include/Sequence.hpp"

using string = std::string;
using timestamp = std::chrono::high_resolution_clock::time_point;

string get_str(const char *prompt) {
    printf("%s", prompt);
    string str;
    std::getline(std::cin, str);

    return str;
}

int get_int(const char *prompt) {
    std::stringstream tmp_input;
    tmp_input << get_str(prompt);

    int res; tmp_input >> res;
    return res;
}

int *get_random_array(int length) {
    int *res = new int[length];
    for (int i = 0; i < length; ++i) {
        res[i] = rand();
    }
    return res;
}


int main() {
    char res = 0;

    Stack<ListSequence, int> stack;
    Stack<ListSequence, int> tmp;
    int start, end;
    int index;
    int val;
    int length;
    int cycles;
    int *data;

    while (!res && !std::cin.eof()) {
        string input = get_str("\nCommand (`?` for help): ");
        switch (input[0]) {
        case '?':
        case 'h':
        case 'm':
            printf(
                "Available options:\n"
                "\n"
                "  General:\n"
                "   ? -- get help\n"
                "   q -- quit this program \n"
                "\n"
                "  Stack:\n"
                "   i -- push value\n"
                "   o -- pop value\n"
                "   p -- print stack\n"
                "   r -- read stack\n"
                "   s -- get substack\n"
                "   c -- concatenate stacks\n"
            );
            break;

        case 'q':
            res = 1;
            break;

        case 'i':
            val = get_int("Enter new element value: ");
            stack.push(val);

            puts("\nElement was successfully pushed!");
            break;
        case 'o':
            try {
                std::cout << stack.pop() << "\n";
                puts("\nElement was successfully popped!");
            } catch (std::exception &e) {
                puts("Index out of range");
            }

            break;

        case 'p':
            std::cout << stack << std::endl;
            break;

        case 'r':
            length = get_int("enter the length of sequence: ");

            if (length < 0) {
                puts("negative length detected!");
                break;
            }

            puts("enter the sequence: ");
            for (int i = 0; i < length; ++i) {
                int t; 
                std::cin >> t;
                stack.push(t);
            }
            get_str("");

            break;

        case 's':
            start = get_int("Enter left range: ");
            end = get_int("Enter right range: ");

            try {
                std::cout << stack(start, end) << "\n";
            } catch (std::runtime_error &e) {
                std::cout << e.what() << "\n";
            }

            break;

        case 'c':

            length = get_int("enter the length of a new stack: ");

            puts("enter new sequence: ");
            for (int i = 0; i < length; ++i) {
                int t; 
                std::cin >> t;
                tmp.push(t);
            }
            get_str("");

            std::cout << stack + tmp << "\n";

            input = get_str("Do you want to save the result to buffer [Y/n]?: ");
            if (input[0] == 'Y') stack += tmp;

            puts("OK!");
            break;

        default:
            printf("unknown command `%c`. Enter `m` to get help\n", input[0]);

            break;
        }
    }

    return res;
}