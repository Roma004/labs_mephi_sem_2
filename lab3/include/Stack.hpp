#include "../../lab2/include/Sequence.hpp"
#include <initializer_list>

template<template <class> class U, class T>
class Stack {
private:
    typedef U<T> container;
    typedef typename U<T>::iterator iterator;

    container data; 
public:
    Stack(): data(container()) {}

    template<template <class> class A>
    Stack(Sequence<A<T>> seq): data(seq) {}

    template<template <class> class A>
    Stack(Stack<A, T> &stack): data(stack.get_sequence()) {}

    Stack(std::initializer_list<T> list): data(list) {}

    void push(T item) { data.append(item); }
    T pop() {
        T res = data.last();
        data.del_last();
        return res;
    }

    U<T> &get_sequence() { return this->data; }

    T &head() { return data.last(); }
    int length() { return data.length(); }

    iterator begin() { return data.begin(); }
    iterator end() { return data.end(); }

    iterator begin() const { return data.begin(); }
    iterator end() const { return data.end(); }
    
    Stack<U, T> map(T (*m)(T &)) {
        Stack<U, T> res(*this);
        for (auto it = res.data.begin(); it != res.data.end(); ++it) {
            *it = m(*it);
        }
        return res;
    }

    Stack<U, T> where(bool (*h)(T &)) {
        Stack<U, T> res;
        for (auto &it : this->data) {
            if (h(it)) res.push(it);
        }
        return res;
    }

    T reduce(T (*r)(T &, T &), T start) {
        auto it = this->data.begin(); 
        T res = r(*it, start); ++it;

        for (it; it != this->data.end(); ++it) {
            res = r(*it, res);
        }

        return res;
    }

    int find(U<T> seq) {
        int ind = 0;
        for (auto it = this->begin(); ind < this->length() - seq.length() + 1; ++it, ++ind) {
            bool flag = true;
            for (auto seq_it = seq.begin(), tmp = it; seq_it != seq.end(); ++seq_it, ++tmp) {
                if (tmp == this->end() || *seq_it != *tmp) {
                    flag = false;
                    break;
                }
            }
            if (flag) return ind;
        }

        return -1;
    }

    int find(Stack<U, T> substack) { return find(substack.get_sequence()); }  
    int find(std::initializer_list<T> list) { return find(container(list)); }  

    template<template<class> class A>
    Stack<U, T> &operator+=(Stack<A, T> &stack) { this->data += stack.get_sequence(); return *this; }

    template<template<class> class A>
    bool operator==(Stack<A, T> &stack) { return this->data == stack.get_sequence(); }

    template<template<class> class A>
    bool operator!=(Stack<A, T> &stack) { return !(*this == stack); }

    template<template<class> class A>
    Stack<U, T> operator+(Stack<A, T> &stack) { return Stack<U, T>(this->data + stack.get_sequence()); }

    Stack<U, T> operator()(int start, int end) { return Stack<U, T>(this->data(start, end)); }

    friend std::ostream &operator<<(std::ostream &stream, const Stack<U, T> &stack) {
        for (auto &i : stack.data) stream << i << " ";
        return stream;
    }
};
