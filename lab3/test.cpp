#include "include/Stack.hpp"
#include "../lab2/include/Sequence.hpp"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <assert.h> 
#include <stdexcept>

int neg(int &num) {
    return -num;
}

bool whr(int &num) {
    return num % 2 == 1;
}

int sum(int &fir, int &sec) {
    return fir + sec;
}

int main() {
    puts("constructors test");

    puts("default constructor <a>");
    Stack<ListSequence, int> a;

    ArraySequence<int> seq_a({1, 2, 3, 4, 5});
    puts("constructor from Sequence <b>");
    Stack<ListSequence, int> b(seq_a);

    puts("constructor from std::initializer_list <c>");
    Stack<ArraySequence, int> c({1, 2, 3, 4, 5});

    puts("");

    std::cout << "<a>: " << a << std::endl;
    std::cout << "<b>: " << b << std::endl;
    std::cout << "<c>: " << c << std::endl;

    puts("");

    puts("checking if length() works");

    puts("for stack <a>");
    assert(a.length() == 0);

    puts("for stack <b>");
    assert(b.length() == 5);

    puts("for stack <b>");
    assert(c.length() == 5);

    puts("");

    puts("checking if <b> == <c>");
    assert(b == c);

    puts("checking if <b> != <a>");
    assert(b != a);

    puts("checking if <a> != <c>");
    assert(a != c);

    puts("");

    puts("checking if head() works");
    assert(b.head() == 5);

    puts("checking if head() fails with null stack");
    try {
        std::cout << a.head();
    } catch (std::runtime_error &e) {
        // std::cout << e.what() << "\n";
    }

    puts("checking if push() works");
    b.push(123);

    puts("checking if value <123> pushed in last position");
    assert(b.head() == 123);

    puts("checking if pop() works");
    assert(b.pop() == 123);

    puts("checking if pop() deleted head stack element");
    assert(b.head() == 5);

    puts("");

    puts("checking if map() works");
    Stack<ListSequence, int> mapped = b.map(neg);

    puts("checking if the result of map() is correct");
    for (int i = 0; i < b.length(); ++i) {
        assert(b.pop() == -mapped.pop());
    }

    puts("");

    puts("checking if where() works");
    Stack<ArraySequence, int> whered = c.where(whr);
    
    puts("checking if the result of where() is correct");
    for (int i = 0; i < b.length(); ++i) {
        assert(whered.pop() % 2 == 1);
    }

    puts("");

    puts("checking if reduce() works");
    int reduced = c.reduce(sum, 123);
    int res = 123;

    for (auto &it : c) {
        res += it;
    }
    puts("checking if the result of reduce() is correct");
    assert(reduced == res);

    puts("");

    Stack<ListSequence, int> e({1, 2, 3, 4, 5, 6});
    Stack<ListSequence, int> d({1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6});
    Stack<ArraySequence, int> null;

    puts("checking if concatenation works ");
    puts("<c> + <e> == <d>"); 
    assert(c + e == d);

    puts("<c> + <null> == <c>");
    assert(c + null == c);

    puts("<null> + <c> == <c>");
    assert(null + c == c);

    puts("");

    puts("checking if substring search works");
    puts("<c> is in <d> at index 0");
    assert(d.find(c) == 0);

    puts("<e> is in <d> at index 5");
    std::cout << d.find(e) << "\n";
    assert(d.find(e) == 5);

    puts("<{3, 4, 5, 1}> is in <d> at index 2");
    assert(d.find({3, 4, 5, 1}) == 2);

    puts("<d> is in <d> at index 0");
    assert(d.find(d) == 0);

    puts("<null> is in <d> at index 0");
    assert(d.find(null) == 0);

    puts("<{3, 2, 1}> is not in <d>");
    assert(d.find({3, 2, 1}) == -1);

    puts("");

    puts("checking if slicing works");

    puts("d[e_idx:e_idx+e.length()] == e");
    assert(d(d.find(e), d.find(e)+e.length()) == e);

    puts("d[d_idx:d_idx+d.length()] == d");
    assert(d(d.find(d), d.find(d)+d.length()) == d);

    puts("d[6:4] == null");
    assert(d(6, 4) == null);

    puts("d[100:200] == null");
    assert(d(100, 200) == null);

    puts("d[-100:100] == d");
    assert(d(-100, 100) == d);

    puts("\nDone.");

    return 0;
}