#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <ostream>
#include <ratio>
#include <sstream>
#include <stdexcept>
#include <string>
#include <iostream>
#include <chrono>

#include "include/LinkedList.hpp"
#include "include/Sequence.hpp"

using string = std::string;
using timestamp = std::chrono::high_resolution_clock::time_point;

string get_str(const char *prompt) {
    printf("%s", prompt);
    string str;
    std::getline(std::cin, str);

    return str;
}

int get_int(const char *prompt) {
    std::stringstream tmp_input;
    tmp_input << get_str(prompt);

    int res; tmp_input >> res;
    return res;
}

int *get_random_array(int length) {
    int *res = new int[length];
    for (int i = 0; i < length; ++i) {
        res[i] = rand();
    }
    return res;
}

template<class T>
void run_insertion(int *data, int length, int cycles) {
    int append_data = 123;

    std::chrono::duration<double, std::milli> ins_time;
    std::chrono::duration<double, std::milli> del_time;

    T l(data, length);

    std::cout << "\nAppending speed: 0ms.| Right deletion speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        timestamp t1 = std::chrono::high_resolution_clock::now();
        l.append(append_data);
        ins_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.del_last();
        del_time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rAppending speed: \033[1;37m" << ins_time.count() / i <<  "ms.\033[0m | " 
                  << "Right deletion speed: \033[1;37m" << del_time.count() / i <<  "ms.\033[0m                " << std::flush;
    }


    std::cout << "\nPrepending speed: 0ms. | Left deletion speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        timestamp t1 = std::chrono::high_resolution_clock::now();
        l.prepend(append_data);
        ins_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.del_first();
        del_time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rPrepending speed: \033[1;37m" << ins_time.count() / i <<  "ms.\033[0m | " 
                  << "Left deletion speed: \033[1;37m" << del_time.count() / i <<  "ms.\033[0m                " << std::flush;
    }

    int mid_index = length / 2;

    std::cout << "\nMiddle insertion speed: 0ms. | Middle deletion speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        timestamp t1 = std::chrono::high_resolution_clock::now();
        l.insert(append_data, mid_index);
        ins_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.del(mid_index);
        del_time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rMiddle insertion speed: \033[1;37m" << ins_time.count() / i <<  "ms.\033[0m | " 
                  << "Middle deletion speed:\033[1;37m " << del_time.count() / i <<  "ms.\033[0m                " << std::flush;
    }

    puts("");
}

template<class T>
void run_deletion(int *data, int length, int cycles) {
    std::chrono::duration<double, std::milli> time;
    std::chrono::duration<double, std::milli> slice_time;

    std::cout << "\nLeft half deletion speed: 0ms. | Left half slicing speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        T l(data, length);

        timestamp t1 = std::chrono::high_resolution_clock::now();
        l(0, length/2);
        slice_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.erase(0, length/2);
        time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rLeft half deletion speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m | "
                  << "Left half slicing speed: \033[1;37m" << slice_time.count() / i << "ms.\033[0m          " << std::flush;
    }

    std::cout << "\nRight half deletion speed: 0ms. | Right half slicing speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        T l(data, length);

        timestamp t1 = std::chrono::high_resolution_clock::now();
        l(length/2, length);
        slice_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.erase(length/2, length);
        time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rRight half deletion speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m | "
                  << "Right half slicing speed: \033[1;37m" << slice_time.count() / i << "ms.\033[0m          " << std::flush;
    }

    std::cout << "\nRandom half deletion speed: 0ms. | Random half slicing speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        T l(data, length);

        int start_ind = rand() % (length / 2);

        timestamp t1 = std::chrono::high_resolution_clock::now();
        l(start_ind, start_ind+length/2);
        slice_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.erase(start_ind, start_ind+length/2);
        time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rRandom half deletion speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m | "
                  << "Random half slicing speed: \033[1;37m" << slice_time.count() / i << "ms.\033[0m          " << std::flush;
    }

    std::cout << "\nFully clearing speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        T l(data, length);

        timestamp t1 = std::chrono::high_resolution_clock::now();
        l.clear();
        time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rFully clearing speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m          " << std::flush;
    }

    puts("");
}


template<class T>
void run_concatenation(int *data, int length, int cycles) {
    std::chrono::duration<double, std::milli> time;

    std::cout << "\nConcatenation speed: 0ms." << std::flush;
    T l(data, length), s(data, length);
    for (int i = 1; i <= cycles; ++i) {

        timestamp t1 = std::chrono::high_resolution_clock::now();
        l + s;
        time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rConcatenation speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m          " << std::flush;
    }
}


void range_deletion_timing(int *data, int length, int cycles) {
    puts("\n\033[1;34mRange deletion and slicing timing tests interface\033[0m");

    puts("\n\033[0;32mStarting range deletion timing tests for ListSequence\033[0m");
    run_deletion<ListSequence<int>>(data, length, cycles);

    puts("\n\033[0;32mStarting range deletion timing tests for ArraySequence\033[0m");
    run_deletion<ArraySequence<int>>(data, length, cycles);
}

void insertion_timing(int *data, int length, int cycles) {

    puts("\n\033[1;34mInsertion timing tests interface\033[0m");
    
    puts("\n\033[0;32mStarting insertion timing tests for ListSequence\033[0m");
    run_insertion<ListSequence<int>>(data, length, cycles);

    puts("\n\033[0;32mStarting insertion timing tests for ArraySequence\033[0m");
    run_insertion<ArraySequence<int>>(data, length, cycles);
}

void concatenation_timing(int *data, int length, int cycles) {

    puts("\n\033[1;34mConcatenation timing tests interface\033[0m");
    
    puts("\n\033[0;32mStarting concatenation timing tests for ListSequence\033[0m");
    run_concatenation<ListSequence<int>>(data, length, cycles);

    puts("\n\033[0;32mStarting concatenation timing tests for ArraySequence\033[0m");
    run_concatenation<ArraySequence<int>>(data, length, cycles);
}

int main() {
    char res = 0;

    ArraySequence<int> arr = ArraySequence<int>();
    ArraySequence<int> tmp = ArraySequence<int>();
    int start, end;
    int index;
    int val;
    int length;
    int cycles;
    int *data;

    while (!res && !std::cin.eof()) {
        string input = get_str("\nCommand (`m` for help): ");
        switch (input[0]) {
        case 'm':
            printf(
                "Available options:\n"
                "\n"
                "  General:\n"
                "   m -- print this menue\n"
                "   q -- stop this program \n"
                "\n"
                "  Sequence:\n"
                "   i -- insert new element into sequence\n"
                "   p -- print sequence\n"
                "   r -- read sequence\n"
                "   d -- delete range of elements\n"
                "   s -- get slice of sequence\n"
                "   c -- concatenate sequences\n"
                "\n"
                "  Timing:\n"
                "   ti -- insertrion and deletion tests\n"
                "   td -- range deletion and slicing tests\n"
                "   tc -- concatenation tests\n"
                "   ta -- start all tests\n"
            );
            break;

        case 'q':
            res = 1;
            break;

        case 'i':
            printf("Enter the index (indeger from 0 to %d): ", arr.length());
            index = get_int("");

            if (index < 0 || index > arr.length()) {
                puts("invalid index");
                break;
            }

            val = get_int("Enter new element value: ");
            
            try {
                if (index == 0) arr.prepend(val);
                else if (index == arr.length()) arr.append(val);
                else arr.insert(val, index);
            } catch (std::exception &e) {
                puts(e.what());
                break;
            }

            puts("\nElement was successfully inserted!");
            break;

        case 'p':
            std::cout << arr << std::endl;
            break;

        case 'r':
            length = get_int("enter the length of sequence: ");

            if (length < 0) {
                puts("negative length detected!");
                break;
            }

            arr.set_length(length);
            puts("enter the sequence: ");
            std::cin >> arr;
            get_str("");

            break;
            
        case 'd':
            start = get_int("Enter left range: ");
            end = get_int("Enter right range: ");

            try {
                arr.erase(start, end);
            } catch (std::runtime_error &e) {
                puts(e.what());
                break;
            }

            puts("\nElements were successfully deleted!");
            break;

        case 's':

            start = get_int("Enter left range: ");
            end = get_int("Enter right range: ");

            std::cout << arr(start, end);

            break;

        case 'c':

            length = get_int("enter the length of a new sequence: ");
            tmp.set_length(length);

            puts("enter new sequence: ");
            std::cin >> tmp;
            get_str("");

            std::cout << arr + tmp << "\n";

            input = get_str("Do you want to save the result to buffer [Y/n]?: ");
            if (input[0] == 'Y') arr += arr + tmp;

            puts("OK!");
            break;

        case 't':

            do length = get_int("Enter the length of sequence: "); while (length < 0);
            do cycles = get_int("Enter the number of test cycles: "); while (length < 0);

            srand(time(NULL));
            data = get_random_array(length);
        
            switch (input[1]) {
            case 'i':
                insertion_timing(data, length, cycles);
                break;

            case 'd':
                range_deletion_timing(data, length, cycles);
                break;

            case 'c':
                concatenation_timing(data, length, cycles);
                break;

            case 'a':
                insertion_timing(data, length, cycles);
                range_deletion_timing(data, length, cycles);
                concatenation_timing(data, length, cycles);

                break;

            default:
                printf("unknown timing command `%c`. Enter `m` to get help\n", input[1]);
            }

            delete [] data;
            break;

        default:
            printf("unknown command `%c`. Enter `m` to get help\n", input[0]);

            break;
        }
    }

    return res;
}