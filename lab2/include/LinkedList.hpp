#pragma once

#include <cstddef>
#include <initializer_list>
#include <stdexcept>

/////////////////////////// LINKED LIST ////////////////////////////

template <class T>
class LinkedList {
private:

    struct item_wrapper;

    item_wrapper *list_head = nullptr;
    item_wrapper *list_tail = nullptr;
    int list_size = 0;

    item_wrapper *get_ptr_by_index(int index);

public:
    class iterator;
    
    LinkedList(T *items, int number);
    LinkedList()
        : list_head(nullptr)
        , list_tail(nullptr)
        , list_size(0) {}
    LinkedList(const LinkedList<T> &list);
    LinkedList(std::initializer_list<T> list);

    ~LinkedList();

    T &head() { return list_head->value; }
    T &tail() { return list_tail->value; }
    int size() { return list_size; }

    void addl(T &item);
    void addr(T &item);

    void insert(T &item, int index);

    void erase(int from, int to);

    T &operator[](int index) { return this->get_ptr_by_index(index)->value; }

    iterator begin() { return iterator(list_head); }
    iterator end() { return iterator(); }

    iterator begin() const { return iterator(list_head); }
    iterator end() const { return iterator(); }
};


/////////////////////////// ITEM WRAPPER ///////////////////////////

template <class T>
struct LinkedList<T>::item_wrapper {
    T value;
    item_wrapper *prev;
    item_wrapper *next;

    item_wrapper(T value, item_wrapper *prev, item_wrapper *next) 
        : value(value)
        , prev(prev)
        , next(next) {}
};

///////////////////////////// ITERATOR /////////////////////////////

template <class T>
struct LinkedList<T>::iterator: public std::iterator<std::input_iterator_tag, T> {
private:

    friend class LinkedList<T>;
    item_wrapper *p;

    iterator(): p(NULL) {}
    iterator(item_wrapper* p): p(p) {}
    iterator(const item_wrapper* p): p(p) {}

public:

    iterator(const iterator &it): p(it.p) {}

    bool operator!=(iterator const& other) const { return p != other.p; }
    bool operator==(iterator const& other) const { return p == other.p; }
    typename iterator::reference operator*() const { return p->value; }
    iterator& operator++() { p = p->next; return *this; }
    iterator& operator--() { p = p->prev; return *this; }
};

/////////////////////////// CONSTRUCTORS ///////////////////////////

template <class T>
LinkedList<T>::LinkedList(T *items, int number) {
    for (T *it = items; it < items + number; ++it) {
        this->addr(*it);
    }
}

template<class T>
LinkedList<T>::LinkedList(const LinkedList<T> &list) {
    for (auto &it : list) {
        this->addr((T &)it);
    }
}

template<class T>
LinkedList<T>::LinkedList(std::initializer_list<T> list) {
    for (auto &it : list) {
        this->addr((T &)it);
    }
}

//////////////////////////// DESTRUCTOR ////////////////////////////

template<class T>
LinkedList<T>::~LinkedList() {
    while (list_tail) {
        item_wrapper *tmp = list_tail->prev;
        delete list_tail;
        list_tail = tmp;
    }
    list_size = 0;
}

///////////////////////////// METHODS //////////////////////////////

template<class T>
void LinkedList<T>::addl(T &item) {
    if (list_size == 0) {
        list_head = new item_wrapper(item, nullptr, nullptr);
        list_tail = list_head;
        list_size = 1;

        return;
    }

    list_head->prev = new item_wrapper(item, nullptr, list_head);
    list_head = list_head->prev;

    ++list_size;
}

template<class T>
void LinkedList<T>::addr(T &item) {
    if (list_size == 0) {
        list_head = new item_wrapper(item, nullptr, nullptr);
        list_tail = list_head;
        list_size = 1;

        return;
    }

    list_tail->next = new item_wrapper(item, list_tail, nullptr);
    list_tail = list_tail->next;

    ++list_size;
}

template<class T>
typename LinkedList<T>::item_wrapper *LinkedList<T>::get_ptr_by_index(int index) {
    if (index < 0 || index >= list_size) {
        throw std::runtime_error("index out of range"); 
    }

    item_wrapper *res;
    if (index < list_size / 2) {
        res = list_head;
        for (int i = 0; i < index; ++i) {
            res = res->next;
        }
    } else {
        res = list_tail;
        for (int i = list_size-1; i > index; --i) {
            res = res->prev;
        }
    }

    return res;
}

template<class T>
void LinkedList<T>::insert(T &item, int index) {
    if (index < 0 || index > list_size) {
        throw std::runtime_error("index out of range"); 
    }

    if (index == 0) {
        this->addl(item);
        return;
    }

    if (index == list_size) {
        this->addr(item);
        return;
    }

    item_wrapper *tmp = this->get_ptr_by_index(index-1);

    tmp->next = new item_wrapper(item, tmp, tmp->next);
    tmp->next->next->prev = tmp->next;

    ++list_size;
}


template <class T>
void LinkedList<T>::erase(int from, int to) {
    item_wrapper *from_ptr = this->get_ptr_by_index(from);
    item_wrapper *prev = from_ptr->prev;

    int deleted = 0;
    while (from < to && from < size()) {
        item_wrapper *tmp = from_ptr->next;
        delete from_ptr;
        from_ptr = tmp;

        ++from;
        ++deleted;
    }

    if (prev) prev->next = from_ptr;
    else list_head = from_ptr;

    if (from_ptr) from_ptr->prev = prev;
    else list_tail = prev;

    list_size -= deleted;
}
