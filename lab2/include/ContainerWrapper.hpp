#pragma once

#include "LinkedList.hpp"
#include "DynamicArray.hpp"
#include "IteratorWrapper.hpp"
#include <cstdio>
#include <initializer_list>

////////////////////////////// CONTAINER_WRAPPER //////////////////////////////

template<class T>
class ContainerWrapper {};

template<class T>
using ArrayContainer = ContainerWrapper<DynamicArray<T>>;

template<class T>
using ListContainer = ContainerWrapper<LinkedList<T>>;

///////////////////////// LIST_WRAPPER_SPECIFICATION  /////////////////////////

template<class T>
class ContainerWrapper<LinkedList<T>> {
protected:
    LinkedList<T> data;

public:
    typedef IteratorWrapper<typename LinkedList<T>::iterator, T> iterator;

    ContainerWrapper(): data(LinkedList<T>()) {}
    ContainerWrapper(LinkedList<T> &list): data(LinkedList<T>(list)) {}
    ContainerWrapper(T *items, int number): data(LinkedList<T>(items, number)) {}

    ContainerWrapper(std::initializer_list<T> list): data(LinkedList<T>(list)) {}

    int length() { return data.size(); }

    T &first() { 
        if (data.size()) return data.head(); 
        throw std::runtime_error("index out of range");
    }
    T &last() { 
        if (data.size()) return data.tail(); 
        throw std::runtime_error("index out of range");
    }

    void append(T &item) { data.addr(item); }
    void prepend(T &item) { data.addl(item); }
    void insert(T &item, int index) { data.insert(item, index); }

    void erase(int from, int to) { data.erase(from, to); }

    void set_length(int length);

    iterator begin() { return iterator(data.begin()); }
    iterator end() { return iterator(data.end()); }

    iterator begin() const { return iterator(data.begin()); }
    iterator end() const { return iterator(data.end()); }

    T &operator[](int index) { return data[index]; }
};

//////////////////////// ARRAY_WRAPPER_SPECIFICATION  /////////////////////////

template<class T>
class ContainerWrapper<DynamicArray<T>> {
protected:
    DynamicArray<T> data;

public:
    typedef IteratorWrapper<typename DynamicArray<T>::iterator, T> iterator;
    
    ContainerWrapper(): data(DynamicArray<T>(0)) {}
    ContainerWrapper(T *items, int number): data(DynamicArray<T>(items, number)) {}
    ContainerWrapper(DynamicArray<T> &array): data(DynamicArray<T>(array.data)) {}

    ContainerWrapper(std::initializer_list<T> list): data(DynamicArray<T>(list)) {}

    int length() { return data.size(); }

    T &first() { 
        if (data.size()) return data[0]; 
        throw std::runtime_error("index out of range");
    }
    T &last() { 
        if (data.size()) return data[data.size()-1]; 
        throw std::runtime_error("index out of range");
    }

    void append(T item) { insert(item, length()); }
    void prepend(T item) { insert(item, 0); }
    void insert(T item, int index);

    void erase(int from, int to);

    void set_length(int length) { data.resize(length); }

    iterator begin() { return iterator(data.begin()); }
    iterator end() { return iterator(data.end()); }

    iterator begin() const { return iterator(data.begin()); }
    iterator end() const { return iterator(data.end()); }


    T &operator[](int index) { return data[index]; }
};

//////////////////////// LIST_WRAPPER_IMPLEMENTATIONS  ////////////////////////

template<class T>
void ListContainer<T>::set_length(int length) {
    if (length < this->length()) {
        erase(length, this->length());
    } else {
        for (int i = this->length(); i < length; ++i) {
            T a = T();
            append(a);
        }
    }
}

/////////////////////// ARRAY_WRAPPER_IMPLEMENTATIONS  ////////////////////////

template<class T>
void ArrayContainer<T>::insert(T item, int index) {
    if (index < 0 || index > length()) {
        throw std::runtime_error("index out of range");
    }
    
    data.resize(length() + 1);

    for (size_t i = length()-1; i > index; --i) {
        data[i] = data[i-1];
    }

    data[index] = item;
}

template<class T>
void ArrayContainer<T>::erase(int from, int to) {
    if (from < 0 || length() <= from || to < 0 || length() < to || from >= to) {
        throw std::runtime_error("index out of range");
    }

    int delta = to-from;

    for (size_t i = to; i < length(); ++i) {
        data[i-delta] = data[i];
    }

    data.resize(length()-delta);
}