#pragma once

#include "ContainerWrapper.hpp"
#include "DynamicArray.hpp"
#include "LinkedList.hpp"
#include <cstdio>
#include <iterator>
#include <ostream>

template<class T>
class Sequence: public ContainerWrapper<T> {
public:
    using ContainerWrapper<T>::ContainerWrapper;
    using ContainerWrapper<T>::begin;
    using ContainerWrapper<T>::end;

    void clear() { this->erase(0, this->length()); }

    void del(int index) { this->erase(index, index+1); }

    void del_first() { del(0); }
    void del_last() { del(this->length()-1); }

    template<class U>
    Sequence(Sequence<U> &seq) {
        this->data = T();

        this->set_length(seq.length());

        auto i = this->begin();
        for (auto &it: seq) {
            *i = it;
            ++i;
        }
    }
    
    Sequence<T> operator()(int start, int end) {
        if (end <= start || end < 0 || start > this->length())
            return Sequence<T>();

        end = std::min(end, this->length());
        start = std::max(0, start);

        Sequence<T> asd = Sequence<T>();
        asd.set_length(end-start);
        auto asd_it = asd.begin();

        int i = 0;
        for (auto it = begin(); i < end; ++it, ++i) {
            if (i >= start) {
                *asd_it = *it;
                ++asd_it;
            }
        }

        return asd;
    }

    friend std::ostream &operator<<(std::ostream &stream, const Sequence<T> &seq) {
        for (auto &i : seq) {
            stream << i << " ";
        }

        return stream;
    }

    friend std::istream &operator>>(std::istream &stream, const Sequence<T> &seq) {
        for (auto it = seq.begin(); it != seq.end(); ++it) {
            stream >> *it;
        }
        return stream;
    }

    template<class U>
    bool operator==(Sequence<U> other) {
        if (this->length() != other.length()) return false;
        
        for (auto a = this->begin(), b = other.begin(); a != this->end(); ++a, ++b) {
            if (*a != *b) return false;
        }

        return true;
    }

    template<class U>
    Sequence<T> operator+(Sequence<U> &a) {
        Sequence<T> res = Sequence<T>();
        res.set_length(this->length() + a.length());
        auto i = res.begin();

        for (auto &it : *this) { *i = it; ++i; }
        for (auto &it : a)     { *i = it; ++i; }

        return res;
    }

    template<class U>
    Sequence<T> &operator+=(Sequence<U> a) {
        int old_length = this->length();
        this->set_length(this->length() + a.length());

        auto i = this->begin();
        for (int asd = 0; asd < old_length; ++asd, ++i);
        for (auto &it : a) { *i = it; ++i; }

        return *this;
    }
};


template<class T>
using ListSequence = Sequence<LinkedList<T>>;

template<class T>
using ArraySequence = Sequence<DynamicArray<T>>;
