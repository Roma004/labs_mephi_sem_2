#pragma once

#include <algorithm>
#include <initializer_list>
#include <stdexcept>
#include <cstring>

/////////////////////////////// DYNAMIC ARRAY ///////////////////////////////

template <class T>
class DynamicArray {
private:
    T *data = nullptr;
    int array_size = 0;

public:
    class iterator;

    DynamicArray(T *items, int number);
    DynamicArray(int size);
    DynamicArray(): DynamicArray(0) {}
    DynamicArray(const DynamicArray<T> &array);
    DynamicArray(std::initializer_list<T> list);

    ~DynamicArray();

    int size();

    void resize(int new_size);

    T &operator[](int index);

    iterator begin() { return iterator(data); }
    iterator end() { return iterator(data + array_size); }

    iterator begin() const { return iterator(data); }
    iterator end() const { return iterator(data + array_size); }
};

/////////////////////////// DYNAMIC ARRAY ITERATOR ///////////////////////////

template <class T>
struct DynamicArray<T>::iterator: public std::iterator<std::input_iterator_tag, T> {
private:

    friend class DynamicArray<T>;
    T *p;

    iterator(T* p): p(p) {}
    iterator(const T* p): p(p) {}

public:

    iterator(const iterator &it): p(it.p) {}

    bool operator!=(iterator const& other) const { return p != other.p; }
    bool operator==(iterator const& other) const { return p == other.p; }
    typename iterator::reference operator*() const { return *p; }
    iterator& operator++() { ++p; return *this; }
    iterator& operator--() { --p; return *this; }
};

//////////////////////////////// CONSTRUCTORS ////////////////////////////////

template<class T>
DynamicArray<T>::DynamicArray(int size) {
    if (size < 0) {
        throw std::runtime_error("negative array size");
    }

    if (size == 0) return;

    data = new T[size];
    memset(data, 0, size*sizeof(T));
    array_size = size;
}

template<class T>
DynamicArray<T>::DynamicArray(T *items, int number) {
    if (number < 0) {
        throw std::runtime_error("negative array size");
    }

    if (number == 0) return;
    
    data = new T[number];
    std::copy(items, items + number, data);
    array_size = number;
}

template<class T>
DynamicArray<T>::DynamicArray(const DynamicArray<T> &array) {
    data = new T[array.array_size];
    std::copy(array.data, array.data + array.array_size, data);

    array_size = array.array_size;
}

template<class T>
DynamicArray<T>::DynamicArray(std::initializer_list<T> list) {
    data = new T[list.size()];
    int i = 0;
    for (auto &it : list) {
        data[i++] = it;
    }

    array_size = list.size();
}

///////////////////////////////// DESTRUCTOR /////////////////////////////////

template<class T>
DynamicArray<T>::~DynamicArray() {
    delete [] data;
    array_size = 0;
}

/////////////////////////// METHODS IMPLEMENTATION ///////////////////////////

template<class T>
int DynamicArray<T>::size() {
    return array_size;
}

template<class T>
void DynamicArray<T>::resize(int new_size) {
    if (new_size < 0) {
        throw std::runtime_error("negative array size");
    }
    T *tmp = data;
    data = new T[new_size];

    std::copy(tmp, tmp + std::min(array_size, new_size), data);
    array_size = new_size;
    
    delete [] tmp;
}

template<class T>
T &DynamicArray<T>::operator[](int index) {
    if (index < 0 || index > array_size) {
        throw std::runtime_error("index out of range");
    }
    
    return data[index];
}
