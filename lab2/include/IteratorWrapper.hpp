#pragma once

#include <iterator>


////////////////////////////// ITERATOR_WRAPPER //////////////////////////////

template<class p_iter, class T>
class IteratorWrapper : public std::iterator<std::input_iterator_tag, T> {
    p_iter p;
public:

    IteratorWrapper(p_iter &it): p(it) {}
    IteratorWrapper(const p_iter &it): p(it) {}
    IteratorWrapper(const IteratorWrapper &it): p(it.p) {}

    bool operator!=(IteratorWrapper const& other) const { return p != other.p; }
    bool operator==(IteratorWrapper const& other) const { return p == other.p; }
    typename IteratorWrapper::reference operator*() const { return *p; }
    IteratorWrapper& operator++() { ++p; return *this; }
    IteratorWrapper& operator--() { --p; return *this; }
};
