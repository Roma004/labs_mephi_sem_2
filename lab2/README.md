# Lab 2

## How to build project

```
git clone https://gitlab.com/Roma004/labs_mephi_sem_2.git
cd lab2
mkdir build
cd build
cmake ..
make
```

## How to run project

To run cli test interface: `/lab2/build/cli`
To run cli test interface: `/lab2/build/test`

## Results of effectivity tests
![effectivity tests](src/timing.png)
