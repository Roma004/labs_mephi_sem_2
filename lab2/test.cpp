#include "include/Sequence.hpp"
#include <assert.h>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>

void test_sequence() {
    puts("test_sequence");

    puts("initialize a from initializer list");
    Sequence<DynamicArray<int>> a = {1, 2, 3, 4, 5};

    puts("initialize l from a");
    ListSequence<int> l = a;

    std::cout << "l: " << l << "\na: " << a << std::endl;
    assert(a == a);
    assert(l == a);

    puts("insert 123 into a by index 2");
    a.insert(123, 2);

    puts("append 345 to a");
    a.append(345);

    puts("prepend 234 to a");
    a.prepend(234);
    assert(a[3] == 123);
    assert(a[0] == 234);
    assert(a[a.length()-1] == 345);

    assert(&a[0] == &*a.begin());
    assert(&a[0] == &a.first());
    assert(&a[a.length()] == &*a.end());
    assert(&a[a.length()-1] == &a.last());

    puts("del first element of a");
    a.del_first();

    puts("del last element of a");
    a.del_last();
    
    puts("del element from a by index 2");
    a.del(2);
    assert(l == a);

    puts("initialize b from initializer list");
    ArraySequence<int> b = {1, 2, 3, 4, 5, 1, 2, 3, 4, 5};
    assert(a + l == b);
    assert(a == b(0, 5));
    assert(b(2, 3)[0] == 3);
    assert(b(2, 1).length() == 0);

    std::stringstream stream;
    puts("writing b to stream");
    stream << b;

    ListSequence<int> c = ListSequence<int>();
    puts("setting c length to b.length()");
    c.set_length(b.length());

    puts("reading b from stream to c");
    stream >> c;

    std::cout << "c: " << c << "\nb: " << b << std::endl;
    assert(c == b);

    puts("clearing c");
    c.clear();
    assert(c.length() == 0);

    puts("Initialize d as null sequence");
    ListSequence<int> d;
    assert(a + d == a);
}


int main() {

    test_sequence();

    puts("\nAll tests passed!");

    return 0;
}