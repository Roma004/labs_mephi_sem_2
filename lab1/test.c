#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "array/array.h"
#include "traceback/traceback.h"

#define EPS 0.00000001

void basic_tests() {
    array *a = array_init();
    array *b = NULL;

    cell_type Error_type = {};

// ---------- value insertions ----------
    puts("insert 123 (pos 0)");
    TRY(array_insert(a, 0, Int, 123), return)       // 0
    puts("append 'asd' to `a`");
    TRY(array_append(a, String, "asd"), return)     // 2
    puts("insert 321.123 (pos 1)");
    TRY(array_insert(a, 1, Float, 321.123), return) // 1
    puts("append `a` to `a`");
    TRY(array_append(a, Array, a), return)          // 3

// ---------- insertion check ----------
    int64_t asd;
    lfloat sdf;
    cstr dfg;
    array *c;

    puts("get `int64_t` item from `a` by index 0");
    TRY(asd = *(int64_t*)array_get_item(a, 0), return)
    assert(asd == 123);

    puts("get `lfloat` item from `a` by index 1");
    TRY(sdf = *(lfloat*)array_get_item(a, 1), return)
    assert(asd - 321.123 < EPS);

    puts("get `cstr` item from `a` by index 2");
    TRY(dfg = (cstr)array_get_item(a, 2), return)
    assert(strcmp(dfg, "asd") == 0);
    

// ---------- array element check ----------
    puts("get `array` item from `a` by index 3 as `c`");
    TRY(c = (array *)array_get_item(a, 3), return)

    puts("get `int64_t` item from `c` by index 0");
    TRY(asd = *(int64_t*)array_get_item(c, 0), return)
    assert(asd == 123);

    puts("get `lfloat` item from `c` by index 0");
    TRY(sdf = *(lfloat*)array_get_item(c, 1), return)
    assert(asd - 321.123 < EPS);

    puts("get `cstr` item from `c` by index 0");
    TRY(dfg = (cstr)array_get_item(c, 2), return)
    assert(strcmp(dfg, "asd") == 0);


// ---------- insertion error check ----------
    puts("try to insert value to `a` by index 100");
    TRY(array_insert(a, 100, Int, 123),
        assert(ERROR_OCCURED == INDEX_OUT_OF_RANGE_CODE);
        // traceback_print(); 
        traceback_free(); 
    );

    puts("try to insert value to `a` by index -1");
    TRY(array_insert(a, -1, Int, 123),
        assert(ERROR_OCCURED == INDEX_OUT_OF_RANGE_CODE);
        // print_error(); 
        traceback_free(); 
    )

    puts("try to insert value to `a` by index a->size+1");
    TRY(array_insert(a, array_get_size(a)+1, Int, 123),
        assert(ERROR_OCCURED == INDEX_OUT_OF_RANGE_CODE);
        // print_error(); 
        traceback_free(); 
    )

    puts("try to insert value to nil pointer");
    TRY(array_insert(b, 0, Int, 123),
        assert(ERROR_OCCURED == NIL_POINTER_CODE);
        // print_error(); 
        traceback_free();
    )

    puts("try to insert value of a type with no methods");
    TRY(array_insert(a, 0, Error_type, 123),
        assert(ERROR_OCCURED == NIL_POINTER_CODE);
        // print_error(); 
        traceback_free();
    )

// ---------- element erasion check ----------

    puts("erase item by index 1");
    TRY(array_erase(a, 1), return)

    puts("try to erase item by index 100");
    TRY(array_erase(a, 100),
        assert(ERROR_OCCURED == INDEX_OUT_OF_RANGE_CODE);
        traceback_free();
    )

    puts("check value of item bu index 1");
    TRY(dfg = (cstr)array_get_item(a, 1), return)
    assert(strcmp(dfg, "asd") == 0);

// ---------- print check ----------
    puts("printing array `a`");
    TRY(array_print(a), return); printf("\n");
    puts("printing array `c`");
    TRY(array_print(c), return); printf("\n");


    puts("try to print nil pointer");
    TRY(array_print(b),
        assert(ERROR_OCCURED == NIL_POINTER_CODE);
        // print_error();
        traceback_free();
    )

// ---------- array_free check ----------
    puts("freing array `a`");
    TRY(array_free(a), return)

    puts("try to free nil pointer");
    TRY(array_free(b),
        assert(ERROR_OCCURED == NIL_POINTER_CODE);
        // print_error();
        traceback_free();
    )
    puts("\n\n");
}

void sort_tests() {
// -------------- integers --------------
    array *a = array_init();
    puts("initialising array of integers");
    TRY(array_append(a, Int, 323), return)
    TRY(array_append(a, Int, 124), return)
    TRY(array_append(a, Int, 7231), return)
    TRY(array_append(a, Int, 13), return)
    TRY(array_append(a, Int, 203), return)
    TRY(array_append(a, Int, 203), return)
    TRY(array_append(a, Int, 1), return)
    TRY(array_append(a, Int, -3), return)
    TRY(array_print(a), return) putchar('\n');

    puts("sorting array of integers");
    TRY(array_sort(a), return)
    TRY(array_print(a), return) putchar('\n');

    for (int i = 1; i < array_get_size(a); ++i) {
        int64_t fir, sec;
        TRY(fir = *(int64_t*)array_get_item(a, i-1), return)
        TRY(sec = *(int64_t*)array_get_item(a, i), return)

        assert(fir <= sec);
    }

    TRY(array_free(a), return)
    putchar('\n');
// -------------- floats --------------
    a = array_init();
    puts("initialising array of floats");
    TRY(array_append(a, Float, 323.123), return)
    TRY(array_append(a, Float, 323.124), return)
    TRY(array_append(a, Float, 323.125), return)
    TRY(array_append(a, Float, 124.0), return)
    TRY(array_append(a, Float, 723.91), return)
    TRY(array_append(a, Float, -13.73), return)
    TRY(array_append(a, Float, -13.73), return)
    TRY(array_append(a, Float, 203.123125), return)
    TRY(array_append(a, Float, 1.11111111111), return)
    TRY(array_append(a, Float, -3.000009), return)
    TRY(array_append(a, Float, -3.000008), return)
    TRY(array_print(a), return ) putchar('\n');

    puts("sorting array of floats");
    TRY(array_sort(a), return)
    TRY(array_print(a), return) putchar('\n');

    for (int i = 1; i < array_get_size(a); ++i) {
        lfloat fir, sec;
        TRY(fir = *(lfloat*)array_get_item(a, i-1), return)
        TRY(sec = *(lfloat*)array_get_item(a, i), return)

        assert(fir <= sec);
    }

    TRY(array_free(a), return)
    putchar('\n');

// -------------- strings --------------
    a = array_init();
    puts("initialising array of strings");
    TRY(array_append(a, String, "asd"), return)
    TRY(array_append(a, String, "sdf"), return)
    TRY(array_append(a, String, "sDf"), return)
    TRY(array_append(a, String, "Asd"), return)
    TRY(array_append(a, String, "Asdf"), return)
    TRY(array_append(a, String, "asdf"), return)
    TRY(array_append(a, String, "asdf"), return)
    TRY(array_append(a, String, "123456"), return)
    TRY(array_append(a, String, "1234567"), return)
    TRY(array_append(a, String, "12345"), return)
    TRY(array_print(a), return) putchar('\n');

    puts("sorting array of strings");
    TRY(array_sort(a), return)
    TRY(array_print(a), return) putchar('\n');

    for (int i = 1; i < array_get_size(a); ++i) {
        cstr fir, sec;
        TRY(fir = (cstr)array_get_item(a, i-1), return)
        TRY(sec = (cstr)array_get_item(a, i), return)

        assert(strcmp(fir, sec) <= 0);
    }

    TRY(array_free(a), return);
    putchar('\n');

// -------------- ints and floats --------------
    a = array_init();
    puts("initialising array of integers and floats");
    TRY(array_append(a, Float, 323.123), return)
    TRY(array_append(a, Float, 323.124), return)
    TRY(array_append(a, Float, 323.125), return)
    TRY(array_append(a, Float, 124.0), return)
    TRY(array_append(a, Float, 723.91), return)
    TRY(array_append(a, Float, -13.73), return)
    TRY(array_append(a, Float, 203.123125), return)
    TRY(array_append(a, Float, 1.11111111111), return)
    TRY(array_append(a, Float, -3.000009), return)
    TRY(array_append(a, Float, -3.000008), return)
    TRY(array_append(a, Int, 323), return)
    TRY(array_append(a, Int, 124), return)
    TRY(array_append(a, Int, 7231), return)
    TRY(array_append(a, Int, 13), return)
    TRY(array_append(a, Int, 203), return)
    TRY(array_append(a, Int, 1), return)
    TRY(array_append(a, Int, -3), return)
    TRY(array_print(a), return) putchar('\n');

    puts("sorting array of integers and floats");
    TRY(array_sort(a), return)
    TRY(array_print(a), return) putchar('\n');
    for (int i = 1; i < array_get_size(a); ++i) {
        void *fir, *sec;
        TRY(fir = array_get_item(a, i-1), return)
        TRY(sec = array_get_item(a, i), return)

        array_cell *fir_cell, *sec_cell;
        TRY(fir_cell = array_get_cell(a, i-1), return)
        TRY(sec_cell = array_get_cell(a, i), return)

        char *type_fir, *type_sec;
        TRY(type_fir = cell_get_typename(fir_cell), return)
        TRY(type_sec = cell_get_typename(sec_cell), return)
        // printf("%d: %s %s\n", i, type_fir, type_sec);

        if (strcmp(type_fir, "Int") == 0) {
            if (strcmp(type_sec, "Int") == 0) assert(*(int64_t*)fir < *(int64_t*)sec);
            else assert(*(int64_t*)fir <= *(lfloat*)sec);
        } else {
            if (strcmp(type_sec, "Int") == 0) assert(*(lfloat*)fir < *(int64_t*)sec);
            else assert(*(lfloat*)fir <= *(lfloat*)sec);
        }
    }

    putchar('\n');

// -------------- ints floats and strings --------------
    puts("appending string 'asd'");
    TRY(array_insert(a, 0, String, "asd"), return)
    TRY(array_print(a), return)
    putchar('\n');

    puts("check if error occures on sort");
    array_sort(a);
    // traceback_print();
    assert(ERROR_OCCURED == NOT_SUPPORTED_OPERATION_CODE);
    traceback_free();
    puts("OK!\n");

    TRY(array_free(a), return);

// -------------- arrays --------------

    a = array_init();
    array *b = array_init();

    TRY(array_append(b, Int, 123), return)
    // append [123]
    TRY(array_append(a, Array, b), return)

    // append [123, 123]
    TRY(array_append(b, Int, 123), return)
    TRY(array_append(a, Array, b), return)

    TRY(array_append(b, Float, 12.3), return)
    //append [123, 123, 12.3]
    TRY(array_append(a, Array, b), return)

    TRY(array_erase(b, array_get_size(b)-1), return)
    TRY(array_insert(b, 0, Float, 1.23), return)
    //append [1.23, 123, 123]
    TRY(array_append(a, Array, b), return)
    TRY(array_print(a), return) putchar('\n');


    TRY(array_sort(a), return)
    TRY(array_print(a), return) putchar('\n');

    array_free(a);
    array_free(b);

    puts("\n\n");
}

array_cell *string_upper(array_cell *cell) {
    EXCEPT(!cell, NIL_POINTER; return NULL)

    char *cell_typename = cell_get_typename(cell);
    EXCEPT(strcmp(cell_typename, "String") != 0, TYPE_ERROR; return NULL )

    cell_type *cell_type = cell_get_type(cell);

    cstr value = cell_get_value(cell);
    size_t len = strlen(value)+1;
    cstr new_value = (cstr)malloc(len*sizeof(char));

    for (size_t i = 0; i < len-1; ++i) {
        if ('a' <= value[i] && value[i] <= 'z') {
            new_value[i] = value[i] - 'a' + 'A';
        } else {
            new_value[i] = value[i];
        }
    }
    new_value[len-1] = '\0';

    array_cell *res;
    TRY(res = cell_init(cell_type, new_value), CONTINUE_ERROR; return NULL )
    free(new_value);

    return res;
}

void map_tests() {
    array *a = array_init();

    puts("initialising array of strings");
    TRY(array_append(a, String, "asd"), return)
    TRY(array_append(a, String, "sdf"), return)
    TRY(array_append(a, String, "sDf"), return)
    TRY(array_append(a, String, "Asd"), return)
    TRY(array_append(a, String, "Asdf"), return)
    TRY(array_append(a, String, "asdf"), return)
    TRY(array_append(a, String, "asdf"), return)
    TRY(array_append(a, String, "123456"), return)
    TRY(array_append(a, String, "1234567"), return)
    TRY(array_append(a, String, "12345"), return)
    TRY(array_print(a), return) putchar('\n');

    puts("mapping array of strings");

    array *b;
    TRY(b = array_map(string_upper, a), return)
    TRY(array_print(b), return) putchar('\n');

    for (size_t i = 0; i < array_get_size(b); ++i) {
        cstr b_dest, a_dest;
        TRY(a_dest = (cstr)array_get_item(a, i), return)
        TRY(b_dest = (cstr)array_get_item(b, i), return)

        assert(strlen(a_dest) == strlen(b_dest));

        for (size_t j = 0; j < strlen(b_dest); ++j) {

            if ('a' <= a_dest[j] && a_dest[j] <= 'z') {
                assert(a_dest[j] == b_dest[j] - 'A' + 'a');
            } else {
                assert(a_dest[j] == b_dest[j]);
            }
        }
    }
    TRY(array_free(b), return);
    putchar('\n');

// ------------ not-valid type ------------

    puts("adding integer to an array");
    TRY(array_insert(a, 0, Int, 12345), return)
    TRY(array_print(a), return) putchar('\n');

    puts("check if type error occures");
    b = array_map(string_upper, a);
    assert(ERROR_OCCURED == TYPE_ERROR_CODE);
    // print_error();
    traceback_free();

    puts("OK!\n\n");

    TRY(array_free(a), return)
}

int is_negative(array_cell *cell) {
    EXCEPT(!cell, NIL_POINTER; return 0)

    char *cell_typename = cell_get_typename(cell);
    if (strcmp(cell_typename, "Int") == 0) {
        int64_t value = *(int64_t*)cell_get_value(cell);
        return value < 0;
    } else if (strcmp(cell_typename, "Float") == 0) {
        lfloat value = *(lfloat*)cell_get_value(cell);
        return value < 0;
    }

    TYPE_ERROR;
    return 0;
}

void where_tests() {
    array *a = array_init();
    
    puts("initialising array of integers and floats");
    TRY(array_append(a, Float, 323.123), return)
    TRY(array_append(a, Float, -323.124), return)
    TRY(array_append(a, Float, 323.125), return)
    TRY(array_append(a, Float, 124.0), return)
    TRY(array_append(a, Float, 723.91), return)
    TRY(array_append(a, Float, -13.73), return)
    TRY(array_append(a, Float, 203.123125), return)
    TRY(array_append(a, Float, 1.11111111111), return)
    TRY(array_append(a, Float, -3.000009), return)
    TRY(array_append(a, Float, -3.000008), return)
    TRY(array_append(a, Int, 323), return)
    TRY(array_append(a, Int, -7231), return)
    TRY(array_append(a, Int, 13), return)
    TRY(array_append(a, Int, 1), return)
    TRY(array_append(a, Int, -3), return)
    TRY(array_print(a), return) putchar('\n');

    array *b;
    puts("generating an array of negative elements of `a`");
    TRY(b = array_where(is_negative, a), return)

    TRY(array_print(b), return) putchar('\n');
    for (int i = 0; i < array_get_size(b); ++i) {
        void *el;
        TRY(el = array_get_item(b, i), return)

        array_cell *el_cell;
        TRY(el_cell = array_get_cell(b, i), return)

        char *el_type;
        TRY(el_type = cell_get_typename(el_cell), return)

        if (strcmp(el_type, "Int") == 0) {
            assert(*(int64_t*)el < 0);
        } else {
            assert(*(lfloat*)el < 0);
        }
    }

    TRY(array_free(a), return)
    TRY(array_free(b), return)
}

void cat_tests() {
    array *a = array_init(), *b = array_init();
    array *c;
    puts("c = [] + []");
    TRY(c = array_cat(a, b), return)
    array_print(c); puts("");
    TRY(array_free(c), return)

    puts("c = [] + [\"123\", 123]");
    TRY(array_insert(b, 0, Int, 123), return)
    TRY(array_insert(b, 0, String, "123"), return)
    TRY(c = array_cat(a, b), return)
    assert(array_compare(b, c) == 0);
    array_print(c); puts("");
    TRY(array_free(c), return)

    puts("c = [\"123\", 123] + []");
    TRY(c = array_cat(b, a), return)
    assert(array_compare(b, c) == 0);
    array_print(c); puts("");
    TRY(array_free(c), return)

    TRY(array_free(a), return)
    TRY(array_free(b), return)

    puts("OK!");
}

int main() {

    puts("------------- tests Stage basic -------------");
    basic_tests();
    puts("------------- tests Stage sort -------------");
    sort_tests();
    puts("------------- tests Stage map -------------");
    map_tests();
    puts("------------- tests Stage where -------------");
    where_tests();
    puts("------------- tests Stage cat -------------");
    cat_tests();

    return 0;
}
