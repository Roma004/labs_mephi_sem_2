#include "array/array.h"
#include "array/core.h"
#include "traceback/traceback.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

struct complex {
    double re;
    double im;
};

void Complex_print(array_cell *c) {
    struct complex *cm = (struct complex *)c->data;
    if (cm->im < 0) {
        printf("%.2lf%.2lfi", cm->re, cm->im);
    } else {
        printf("%.2lf+%.2lfi", cm->re, cm->im);
    }
}

void Complex_set_length(array_cell *c, generic value) {
    c->data_length = sizeof(struct complex);
}

cell_type Complex = {
    "Complex",
    Complex_set_length,
    __default_set_value,
    Complex_print,
    __default_get_value,
    __default_free_data,
    NULL // comparators
};

int main() {
    array *a;
    TRY(a = array_init(), return 0;)

    struct complex asd = {123, 345};
    TRY(array_append(a, Complex, &asd), return 0)
    asd.re = -2;
    TRY(array_append(a, Complex, &asd), return 0)
    asd.re = 18;
    asd.im = -890.75;
    TRY(array_append(a, Complex, &asd), return 0)
    TRY(array_insert(a, 100, Complex, &asd), return 0)

    TRY(array_print(a), return 0) putchar('\n');

    TRY(array_free(a), return 0)

    return 0;
}