#include <bits/thread-shared-types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core.h"
#include "structs.h"


void *__default_get_value(array_cell *c) {
    return c->data;
}

void __default_set_value(array_cell *c, generic val) {
    memcpy(c->data, val, c->data_length);
}

void __default_free_data(array_cell *c) {
    free(c->data);
}

/* ----------------------  INT  ---------------------- */
void __Int_print(array_cell *c) {
    printf("%ld", *(int64_t*)(c->data));
}

void __Int_set_length(array_cell *c, generic value) {
    c->data_length = sizeof(int64_t);
}

int __Int_Int_comparator(array_cell *a, array_cell *b) {
    int64_t a_val = *(int64_t*)a->data, b_val = *(int64_t*)b->data;
    if (a_val < b_val) return -1;
    if (a_val > b_val) return 1;
    return 0;
}

int __Int_Float_comparator(array_cell *a, array_cell *b) {
    int64_t a_val = *(int64_t*)a->data;
    lfloat b_val = *(lfloat*)b->data;
    if (a_val < b_val) return -1;
    if (a_val > b_val) return 1;
    return 0;
}


cell_type Int = {
    "Int",
    __Int_set_length,
    __default_set_value,
    __Int_print,
    __default_get_value,
    __default_free_data,
    (struct comparator_wrapper *[3]) {
        &(struct comparator_wrapper){"Int", __Int_Int_comparator},
        &(struct comparator_wrapper){"Float", __Int_Float_comparator},
        NULL
    }
};

/* ---------------------  FLOAT  --------------------- */

void __Float_print(array_cell *c) {
    printf("%lf", *(double*)c->data);
}

void __Float_set_length(array_cell *c, generic value) {
    c->data_length = sizeof(lfloat);
}

int __Float_Float_comparator(array_cell *a, array_cell *b) {
    lfloat a_val = *(lfloat*)a->data;
    lfloat b_val = *(lfloat*)b->data;
    if (a_val < b_val) return -1;
    if (a_val > b_val) return 1;
    return 0;
}

int __Float_Int_comparator(array_cell *a, array_cell *b) {
    lfloat a_val = *(lfloat*)a->data;
    int64_t b_val = *(int64_t*)b->data;
    if (a_val < b_val) return -1;
    if (a_val > b_val) return 1;
    return 0;
}

cell_type Float = {
    "Float",
    __Float_set_length,
    __default_set_value,
    __Float_print,
    __default_get_value,
    __default_free_data,
    (struct comparator_wrapper *[3]) {
        &(struct comparator_wrapper){"Float", __Float_Float_comparator},
        &(struct comparator_wrapper){"Int", __Float_Int_comparator},
        NULL
    }
};

/* ---------------------  STRING  -------------------- */


void __String_set_length(array_cell *c, generic value) {
    c->data_length = (strlen(value)+1)*sizeof(char);
}

void __String_print(array_cell *c) {
    printf("\"%s\"", (cstr)c->data);
}

int __String_String_comparator(array_cell *a, array_cell *b) {
    return strcmp(a->data, b->data);
}

cell_type String = {
    "String",
    __String_set_length,
    __default_set_value,
    __String_print,
    __default_get_value,
    __default_free_data,
    (struct comparator_wrapper *[2]) {
        &(struct comparator_wrapper){"String", __String_String_comparator},
        NULL
    }
};

/* ---------------------  ARRAY  --------------------- */

void __Array_set_length(array_cell *c, generic value) {
    c->data_length = sizeof(array);
}

void __Array_set_value(array_cell *c, void *val) {
    array *tmp = array_copy(val);
    memcpy(c->data, tmp, sizeof(array));
    free(tmp);
}

void __Array_print(array_cell *c) {
    array_print(c->data);
}

void __Array_free_data(array_cell *c) {
    array_free(c->data);
}

int __Array_Array_comparator(array_cell *a, array_cell *b) {
    return array_compare(a->data, b->data);
}

cell_type Array = {
    "Array",
    __Array_set_length,
    __Array_set_value,
    __Array_print,
    __default_get_value,
    __Array_free_data,
    (struct comparator_wrapper *[2]) {
        &(struct comparator_wrapper){"Array", __Array_Array_comparator},
        NULL
    }
};
