#include "structs.h"
#ifndef ARRAY_ALLOC_SIZE
#define ARRAY_ALLOC_SIZE 10
#endif

#ifndef ARRAY_REALLOC_COEFFICIENT
#define ARRAY_REALLOC_COEFFICIENT 2
#endif

#ifndef ARRAY_PRINT_DELIMITER
#define ARRAY_PRINT_DELIMITER ", "
#endif

struct comparator_wrapper;

typedef struct cell_type cell_type;
typedef struct array_cell array_cell;
typedef struct array array;

typedef double lfloat;
typedef char* cstr;
typedef void* generic;