#pragma once

#include <stddef.h>
#include <stdint.h>

struct array_cell {
    void *data;
    size_t data_length;
    struct cell_type *_type;
};

struct comparator_wrapper {
    char *compare_type_name;
    int (*cmparator)(struct array_cell *, struct array_cell *);
};

struct cell_type {
    char *_typename;
    void (*set_length)(struct array_cell *, void *);
    void (*set_value)(struct array_cell *, void *);
    void (*print)(struct array_cell *);
    void *(*get_value)(struct array_cell *);
    void (*free_data)(struct array_cell *);
    struct comparator_wrapper **comparators;
};

struct array {
    struct array_cell **data;
    size_t mem_allocated;
    size_t size;
};