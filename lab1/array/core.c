#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../traceback/traceback.h"

#include "structs.h"
#include "core.h"


#define FNCNAME "array_init"
array *array_init() {
    array *res = (array *)malloc(sizeof(array));
    EXCEPT( !res, ALLOC_ERROR; return NULL )

    res->mem_allocated = ARRAY_ALLOC_SIZE;
    res->size = 0;

    res->data = (array_cell **)calloc(res->mem_allocated, sizeof(array_cell *));
    EXCEPT( !res->data, free(res); ALLOC_ERROR; return NULL )

    return res;
}
#undef FNCNAME

#define FNCNAME "array_copy"
array *array_copy(array *arr) {
    EXCEPT( !arr, NIL_POINTER; return NULL )

    array *res = (array *)malloc(sizeof(array));
    EXCEPT( !res, ALLOC_ERROR; return NULL )

    memcpy(res, arr, sizeof(array));
    res->data = (array_cell **)calloc(res->mem_allocated, sizeof(array_cell *));
    EXCEPT( !res->data, free(res); ALLOC_ERROR; return NULL )

    for (size_t i = 0; i < arr->size; ++i) {
        TRY(res->data[i] = cell_init(arr->data[i]->_type, arr->data[i]->data),
            free(res->data); 
            free(res);
            return NULL
        )
    }

    return res;
}
#undef FNCNAME

#define FNCNAME "__array_resize_if_need"
void __array_resize_if_need(array *arr) {
    EXCEPT( !arr, NIL_POINTER; return )
    
    if (arr->size == arr->mem_allocated) {
        arr->mem_allocated *= ARRAY_REALLOC_COEFFICIENT;

        arr->data = (array_cell **)realloc(arr->data, arr->mem_allocated*sizeof(array_cell *));
        EXCEPT( !arr->data, ALLOC_ERROR; return );
    }
}
#undef FNCNAME 

#define FNCNAME "__cell_init"
array_cell *cell_init(cell_type *_t, generic value) {
    EXCEPT( !_t, NIL_POINTER; return NULL )

    array_cell *dest = (array_cell *)calloc(1, sizeof(array_cell));
    EXCEPT( !dest, ALLOC_ERROR; return NULL )

    cell_set_type(dest, _t);

    TRY( cell_check_method(dest, set_length), 
        free(dest);
        return NULL 
    )
    cell_call_method(dest, set_length(dest, value));

    dest->data = malloc(dest->data_length);
    EXCEPT( !dest->data, free(dest); ALLOC_ERROR; return NULL );

    TRY( cell_check_method(dest, set_value), 
        free(dest->data);
        free(dest);
        
        return NULL 
    )
    cell_call_method(dest, set_value(dest, value));

    return dest;
}
#undef FNCNAME

#define FNCNAME "__check_valid_type"
void __check_valid_type(cell_type *_t, generic value) {
    EXCEPT( !_t, NIL_POINTER; return )

    TRY( type_check_method(_t, set_length),  return )
    TRY( type_check_method(_t, set_value),  return )
    TRY( type_check_method(_t, print),  return )
    TRY( type_check_method(_t, get_value),  return )
    TRY( type_check_method(_t, free_data),  return )
}
#undef FNCNAME

#define FNCNAME "array_insert_cell"
void array_insert_cell(array *arr, size_t index, array_cell *value) {
    EXCEPT( !arr, NIL_POINTER; return )
    EXCEPT( !value, NIL_POINTER; return )

    EXCEPT( index < 0 || index > arr->size, INDEX_OUT_OF_RANGE; return )

    TRY( __array_resize_if_need(arr),  return ) 

    for (size_t i = arr->size; i > index; --i) {
        arr->data[i] = arr->data[i-1];
    }

    arr->data[index] = value;
    ++arr->size;
}
#undef FNCNAME

#define FNCNAME "__array_insert"
void __array_insert(array *arr, size_t index, cell_type *_t, generic value) {
    EXCEPT( !arr, NIL_POINTER; return )
    EXCEPT( !_t, NIL_POINTER; return )

    TRY(__check_valid_type(_t, value),  return )

    array_cell *new_cell;
    TRY( new_cell = cell_init(_t, value),  return )

    TRY( array_insert_cell(arr, index, new_cell), 
        TRY(cell_free(new_cell), 
            free(new_cell->data);
            free(new_cell);
            return
        )
        
        return 
    )
}
#undef FNCNAME

void __array_insert_double_wrapper(array *arr, size_t index, cell_type *_t, double value) {
    double tmp = (double)value;
    __array_insert(arr, index, _t, &tmp);
}

void __array_insert_int_wrapper(array *arr, size_t index, cell_type *_t, long long value) {
    __array_insert(arr, index, _t, &value);
}

void __array_insert_pointer_wrapper(array *arr, size_t index, cell_type *_t, generic value) {
    __array_insert(arr, index, _t, value);
}


#define FNCNAME "array_erase"
void array_erase(array *arr, size_t index) {
    EXCEPT( !arr, NIL_POINTER; return )
    EXCEPT( index < 0 || index >= arr->size, INDEX_OUT_OF_RANGE; return )

    TRY( cell_free(arr->data[index]),  return )

    for (size_t i = index; i < arr->size-1; ++i) {
        arr->data[i] = arr->data[i+1];
    }
    --arr->size;
}
#undef FNCNAME

void cell_free(array_cell *_cell) {
    TRY( cell_check_method(_cell, free_data), return )
    cell_call_method(_cell, free_data(_cell));

    free(_cell);
}

#define FNCNAME "array_free"
void array_free(array *arr) {
    EXCEPT( !arr, NIL_POINTER; return )

    for (size_t i = 0; i < arr->size; ++i) {
        cell_free(arr->data[i]);
    }
    free(arr->data);    
    free(arr);
}
#undef FNCNAME


#define FNCNAME "array_print"
void array_print(array *arr) {
    EXCEPT( !arr, NIL_POINTER; return )

    printf("[");
    for (int i = 0; i < arr->size; ++i) {
        TRY( cell_check_method(arr->data[i], print), return )
        TRY( cell_call_method(arr->data[i], print(arr->data[i])), return )
        if (i != arr->size - 1) {
            printf(ARRAY_PRINT_DELIMITER);
        }
    }
    printf("]");
}
#undef FNCNAME


size_t array_get_size(array *arr) {
    EXCEPT(!arr, NIL_POINTER; return 0)
    return arr->size;
}

array_cell *array_get_cell(array *arr, size_t ind) {
    EXCEPT(!arr, NIL_POINTER; return NULL)
    EXCEPT(array_get_size(arr) <= ind, INDEX_OUT_OF_RANGE; return NULL)

    return arr->data[ind];
}

array_cell **array_get_cell_ptr(array *arr, size_t ind) {
    EXCEPT(!arr, NIL_POINTER; return NULL)
    EXCEPT(array_get_size(arr) <= ind, INDEX_OUT_OF_RANGE; return NULL )

    return &arr->data[ind];
}

generic array_get_item(array *arr, size_t ind) {
    array_cell *cell;
    TRY(cell = array_get_cell(arr, ind), return NULL)
    
    generic res;
    TRY(res = cell_get_value(cell), return NULL)
    
    return res;
}

int (*cell_get_comparator(array_cell *_cell, char *typename))(array_cell *, array_cell *) {
    EXCEPT(!_cell, NIL_POINTER; return NULL)

    struct comparator_wrapper **iterator = _cell->_type->comparators;
    if (!iterator) return NULL;

    while (*iterator) {
        if (strcmp((*iterator)->compare_type_name, typename) == 0) {
            return (*iterator)->cmparator;
        }
        ++iterator;
    }

    return NULL;   
}

int cell_compare(array_cell *a, array_cell *b) {
    EXCEPT(!a, NIL_POINTER; return 0 )
    EXCEPT(!b, NIL_POINTER; return 0 )

    int (*cmp)(array_cell *, array_cell *);
    TRY(cmp = cell_get_comparator(a, b->_type->_typename), return 0 )

    EXCEPT(!cmp,
        char exc[150];
        snprintf(
            exc, 150, "compare operation is not supported between types %s and %s", 
            a->_type->_typename, b->_type->_typename
        );
        traceback_start(exc, NOT_SUPPORTED_OPERATION_CODE);
        return 0;
    )

    return cmp(a, b);
}

generic cell_get_value(array_cell *_cell) {
    TRY(cell_check_method(_cell, get_value), return NULL)

    return cell_call_method(_cell, get_value(_cell));
}


void cell_set_type(array_cell *_cell, cell_type *_type) {
    EXCEPT(!_cell, NIL_POINTER; return)
    EXCEPT(!_type, NIL_POINTER; return)
    
    _cell->_type = _type;
}

char *cell_get_typename(array_cell *_cell) {
    cell_type *type;
    TRY(type = cell_get_type(_cell), return NULL)

    char *typename;
    TRY(typename = type_get_typename(type), return NULL)

    return typename;
}

char *type_get_typename(cell_type *_type) {
    EXCEPT(!_type, NIL_POINTER; return NULL)

    return _type->_typename;
}

cell_type *cell_get_type(array_cell *_cell) {
    EXCEPT(!_cell, NIL_POINTER; return NULL)

    return _cell->_type;
}
