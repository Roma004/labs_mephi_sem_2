#include "../traceback/traceback.h"
#include "core.h"
#include "string.h"
#include <stddef.h>

void array_sort(array *arr) {
    EXCEPT(!arr, NIL_POINTER)
    
    if (array_get_size(arr) == 0) return;

    size_t size = array_get_size(arr);
    for (size_t i = 0; i < size - 1; ++i) {
        size_t min_ind = i;

        for (size_t j = i + 1; j < size; ++j) {
            array_cell *a = array_get_cell(arr, j);
            array_cell *b = array_get_cell(arr, min_ind);
            
            int flag;
            TRY(flag = cell_compare(a, b), CONTINUE_ERROR; return);

            if (flag < 0) {
                min_ind = j;
            }
        }
        if (i != min_ind) {

            array_cell *tmp = array_get_cell(arr, i);
            
            array_cell **asd = array_get_cell_ptr(arr, i);
            *asd = array_get_cell(arr, min_ind);
            
            asd = array_get_cell_ptr(arr, min_ind);
            *asd = tmp;
        }
    }
}

array *array_map(array_cell *(*f)(array_cell *), array *arr) {
    EXCEPT(!arr, NIL_POINTER; return NULL)
    EXCEPT(!f, NIL_POINTER; return NULL)

    array *res;
    TRY(res = array_init(), CONTINUE_ERROR; return NULL)
    size_t size = array_get_size(arr);
    for (size_t i = 0; i < size; ++i) {
        array_cell *dest = array_get_cell(arr, i);

        array_cell *new_cell;
        TRY(new_cell = f(dest), 
            CONTINUE_ERROR;
            array_free(res);
            return NULL
        )
        
        TRY(array_insert_cell(res, array_get_size(res), new_cell), 
            CONTINUE_ERROR;
            array_free(res);
            cell_free(new_cell);
            return NULL;
        )
    } 

    return res;
}

array *array_where(int (*h)(array_cell *), array *arr) {
    EXCEPT(!arr, NIL_POINTER; return NULL)
    EXCEPT(!h, NIL_POINTER; return NULL)


    array *res;
    TRY(res = array_init(), CONTINUE_ERROR; return NULL)
    size_t size = array_get_size(arr);
    for (size_t i = 0; i < size; ++i) {
        array_cell *dest = array_get_cell(arr, i);

        int flag;
        TRY(flag = h(dest), 
            CONTINUE_ERROR;
            array_free(res);
            return NULL
        )

        if (flag) {
            cell_type *cell_type = cell_get_type(dest);
            generic cell_value = cell_get_value(dest);
            array_cell *new_cell;
            TRY(new_cell = cell_init(cell_type, cell_value), 
                CONTINUE_ERROR;
                array_free(res);
                return NULL;
            )
            
            TRY(array_insert_cell(res, array_get_size(res), new_cell), 
                CONTINUE_ERROR;
                array_free(res);
                cell_free(new_cell);
                return NULL;
            )  
        }
    }

    return res;
}

array *array_cat(array *a, array *b) {
    EXCEPT(!a, NIL_POINTER; return NULL)
    EXCEPT(!b, NIL_POINTER; return NULL)

    array *c;
    TRY(c = array_copy(a), CONTINUE_ERROR; return NULL)
    for (size_t i = 0; i < array_get_size(b); ++i) {
        array_cell *cur = array_get_cell(b, i);
        array_cell *new_cell;
        TRY(new_cell = cell_init(cell_get_type(cur), cell_get_value(cur)),
            CONTINUE_ERROR;
            array_free(c);
            return NULL
        )

        TRY(array_insert_cell(c, array_get_size(c), new_cell),
            CONTINUE_ERROR;
            array_free(c);
            cell_free(new_cell);
            return NULL;
        );
    }

    return c;
}

int array_compare(array *a, array *b) {
    EXCEPT(!a, NIL_POINTER; return 0)
    EXCEPT(!b, NIL_POINTER; return 0)

    size_t size_a = array_get_size(a),
           size_b = array_get_size(b);
    
    if (size_a != size_b) return size_a < size_b ? -1 : 1;

    for (size_t i = 0; i < size_a; ++i) {
        array_cell *a_cell = array_get_cell(a, i),
                   *b_cell = array_get_cell(b, i);
        
        cell_type *a_type = cell_get_type(a_cell),
                  *b_type = cell_get_type(b_cell);

        int flag;
        TRY(flag = cell_compare(a_cell, b_cell),
            CONTINUE_ERROR; return 0;    
        );
        if (flag != 0) return flag;
    }
    
    return 0;
}