#pragma once

#include "../traceback/traceback.h"
#include <stdint.h>
#include <stddef.h>

#include "definitions.h"

// ---------------- array ----------------
array *array_init();
array *array_copy(array *arr);

array *array_map(array_cell *(*f)(array_cell *), array *arr);
array *array_where(int (*h)(array_cell *), array *arr);
array *array_cat(array *a, array *b);

void array_insert_cell(array *arr, size_t index, array_cell *value);
void __array_insert_double_wrapper(array *arr, size_t index, cell_type *_t, double value);
void __array_insert_int_wrapper(array *arr, size_t index, cell_type *_t, long long value);
void __array_insert_pointer_wrapper(array *arr, size_t index, cell_type *_t, generic value);

#define array_insert(arr, index, _type, value) (_Generic((value),\
    double: __array_insert_double_wrapper, \
    long long: __array_insert_int_wrapper, \
    int: __array_insert_int_wrapper, \
    default: __array_insert_pointer_wrapper \
)(arr, index, &_type, value)) 

#define array_append(arr, _t, value) (array_insert(arr, array_get_size(arr), _t, value))

size_t array_get_size(array *arr);
generic array_get_item(array *arr, size_t ind);
array_cell *array_get_cell(array *arr, size_t ind);
array_cell **array_get_cell_ptr(array *arr, size_t ind);

void array_print(array *arr);
void array_erase(array *arr, size_t index);
void array_sort(array *arr);

int array_compare(array *a, array *b);

void array_free(array *arr);

// ---------------- cell ----------------

array_cell *cell_init(cell_type *_t, generic value);
void *cell_get_value(array_cell *c);

void cell_set_type(array_cell *_cell, cell_type *_type);
cell_type *cell_get_type(array_cell *_cell);

char *cell_get_typename(array_cell *_cell);

int cell_compare(array_cell *a, array_cell *b);

void cell_free(array_cell *_cell);

#define cell_check_method(_cell, _function_name) { \
    EXCEPT(!_cell, NIL_POINTER) \
    if (!ERROR_OCCURED) EXCEPT(!_cell->_type, NIL_POINTER) \
    if (!ERROR_OCCURED) type_check_method(_cell->_type, _function_name) \
}

#define cell_call_method(_cell, _function) ((_cell)->_type->_function)

// ---------------- type ----------------

char *type_get_typename(cell_type *_type);

// TODO say what methpd is missing
#define type_check_method(_type, _function_name) {\
    EXCEPT(!_type->_function_name, NIL_POINTER) \
}
