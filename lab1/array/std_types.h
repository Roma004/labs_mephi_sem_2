#pragma once

#include "definitions.h"

extern cell_type Int;
extern cell_type Float;
extern cell_type String;
extern cell_type Array;

void *__default_get_value(array_cell *c);
void __default_set_value(array_cell *c, generic val);
void __default_free_data(array_cell *c);