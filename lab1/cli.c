#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "array/array.h"
#include "traceback/traceback.h"


char *get_str(char *prompt) {
    printf("%s", prompt);
    char buf[81] = {};
    char *res = NULL;
    int len = 0;
    int n = 0;
    do {
        n = scanf("%80[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        } else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        } else {
            scanf("%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    } else {
        res = calloc(1, sizeof(char));
    }

    return res;
}


int main() {
    char res = 0;
    static char order;

    FILE *file;
    array *arr = array_init();

    cell_type *types[4] = {
        &Int, &Float, &String, NULL 
    };

    char *input = 0;
    while (!res && (input = get_str("\nCommand (`m` for help): "))) {
        switch (input[0]) {
        case 'm':
            printf(
                "Available options:\n"
                "\n"
                "  General:\n"
                "   m -- print this menue\n"
                "   q -- stop this program \n"
                "\n"
                "  Array:\n"
                "   i -- insert new element into array by index\n"
                "   p -- print array\n"
                "   c -- clear array\n"
                "   d -- delete element by index\n"
                "   s -- sort an array\n"
            );
            break;

        case 'q':
            res = 1;
            break;

        case 'i':;
            char *type_str = get_str("type of new elenemt: (String, Int, Float): ");
            if (!type_str) {
                break;
            }

            cell_type **type = NULL;
            for (type = types; *type; ++type) {
                char *typename = type_get_typename(*type);
                if (strcmp(type_str, typename) == 0) {
                    free(type_str);
                    break;
                }
            }

            if (!*type || ERROR_OCCURED) {
                printf("\n**ERROR**: unknown type `%s`\n", type_str);
                free(type_str);
                break;
            }
            size_t max_ind = array_get_size(arr), size;

            int ind = 0;
            char buf[60];
            sprintf(buf, "Enter the index(integer from 0 to %ld): ", max_ind);
            char *index_str = get_str(buf);
            if (!index_str) break;

            ind = strtol(index_str, NULL, 10);
            if (errno) {
                printf("\n**ERROR**: bad integer: `%s\n`", index_str);
                free(index_str);
                break;
            }
            free(index_str);

            if (!(0 <= ind && ind <= array_get_size(arr))) {
                printf("\n**ERROR**: index out of range\n");
                break;
            }

            char *element = get_str("enter new element: ");
            if (*type == &Int) {
                int val = strtol(element, NULL, 10);
                if (errno) {
                    printf("\n**ERROR**: bad integer: `%s\n`", index_str);
                    free(element);
                    break;
                }
                array_insert(arr, ind, Int, val);
            } else if (*type == &Float) {
                lfloat val = atof(element);
                if (errno) {
                    printf("\n**ERROR**: bad float: `%s\n`", index_str);
                    free(element);
                    break;
                }
                array_insert(arr, ind, Float, val);
            } else {
                array_insert(arr, ind, String, element);
            }
            free(element);

            puts("\nElement was successfully inserted!");
            break;

        case 'p':
            array_print(arr);
            break;

        case 'c':
            for (int i = array_get_size(arr)-1; i >= 0; --i) {
                array_erase(arr, i);
            }
            puts("Array was successfully cleared");
            break;

        case 'd':
            ind = 0;
            max_ind = array_get_size(arr);
            sprintf(buf, "Enter the index(integer from 0 to %ld): ", max_ind-1);
            index_str = get_str(buf);
            if (!index_str) break;

            ind = strtol(index_str, NULL, 0);
            if (errno) {
                printf("\n**ERROR**: bad integer: `%s\n`", index_str);
                free(index_str);
                break;
            }
            free(index_str);

            if (!(0 <= ind && ind < array_get_size(arr))) {
                printf("\n**ERROR**: index out of range\n");
                break;
            }

            array_erase(arr, ind);

            puts("\nElements were successfully deleted!");
            break;

        case 's':
            TRY(array_sort(arr), 
                traceback_print(); traceback_free(); break
            )
            puts("\nArray was successfully sorted");
            break;

        default:
            printf("unknown command `%c`. Enter `m` to get help\n", input[0]);
        }

        free(input);
    }

    array_free(arr);

    return res;
}