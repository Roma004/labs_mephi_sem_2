# Lab1
Написать на языке C реализацию абстрактного типа данных – полиморфной коллекции на
основе динамического массива. Написать программу-оболочку для тестирования этой
реализации. Под полиморфным, в данном случае, подразумевается такой массив, который
может хранить (и обрабатывать) значения различных типов. Что конкретно понимается под
обработкой – зависит от конкретного варианта задания

## Special task (variant 11):

    Тип контейнера: Динамический массив

    Типы хранимых элементов:
    - целые числа
    - строки 

    Опреации:
    - сортировка
    - map, where
    - конкатенация

## what was implemented and tested:

* general
  - values(`Int`, `Float`, `String`) insertion
  - arrays insertion
  - getting items by index
  - item deletion
  - types comparsion
  - arrays comparsion
  - arrays copying
  - printing arrays
* special
  - sort function
  - map function
  - where function
  - arrays concatenation
* exceptions handling
  - index exceptions handling
  - nil pointer exceptions handling
  - allocation errors
  - type errors
* console user interface
  - insertion and deletion for types `Int`, `Float`, `String`
  - printing array
  - sorting array
  - clearing array

## how to build and run the project:

To build the project
```bash
git clone https://gitlab.com/Roma004/labs_mephi_sem_2.git
cd labs_mephi_sem_2/lab1
mkdir build && cd build
cmake ..
make
```

to rebuild project after making changes just `make` in `/lab1/build` directory

run `./test` script to start tests. (tests sources in `test.c` file)

run `./cli` script to start console user interface

## Basic API intriduction

```c++
int main() {
// ---------- array initialization ----------
    array *a;
    /*
    - use a = array_init() to initialize new instance of array

    - use RAISE_ON_ERROR macro to print info about any error occured
      during function call and automaticly stop program execution
    */
    RAISE_ON_ERROR(a = array_init());

// ---------- value insertions ----------
    RAISE_ON_ERROR(array_insert(a, 0, Int, 123))       // insert value `123` of type `Int` by index 0
    RAISE_ON_ERROR(array_append(a, String, "asd"))     // append value `asd` of type `String`
    RAISE_ON_ERROR(array_insert(a, 1, Float, 321.123)) // insert value `321.123` of type `Float` by index 1
    RAISE_ON_ERROR(array_append(a, Array, a))          // append copy of array `a` to array `a`
    /*
    WARNING:
    - during value insertion it's value is copied to the new block of memory, so
    every blocks you allocated manualy you need to free manualy
    - to insert Float value to the array you need to pass `123.0` rather than `123`
    */

// ---------- get value by index ----------
    int64_t asd;
    lfloat sdf;
    cstr dfg;
    array *c;

    RAISE_ON_ERROR(asd = *(int64_t*)array_get_item(a, 0)); // asd == 123
    RAISE_ON_ERROR(sdf = *(lfloat*)array_get_item(a, 1));  // sdf == 321.123
    RAISE_ON_ERROR(dfg = (cstr)array_get_item(a, 2));      // dfg == "asd"
    RAISE_ON_ERROR(c = (array *)array_get_item(a, 3));     // c == [123, 321.123, "asd"]

// ---------- insertion error check ----------
    /*
    when any runtime erorr occures `ERROR_OCCURED` macro is setted in one of this values:
    - ALLOC_ERROR             -- malloc / calloc / realloc call failed
    - INDEX_OUT_OF_RANGE      -- accessing to an array cell by bad index
    - NIL_POINTER             -- one of pointer parameters passed in function is NULL
    - NOT_SUPPORTED_OPERATION -- operation between to cells is unsupported (e.g. comparsion)
    - TYPE_ERROR              -- function expected to get value not of type passed 
    */
    array_insert(a, 100, Int, 123);  // ERROR_OCCURED == INDEX_OUT_OF_RANGE
    array_insert(NULL, 0, Int, 123); // ERROR_OCCURED == NIL_POINTER

    print_error(); // print error traceback to stderr 

    pass_error();  // free memory from error message and set `ERROR_OCCURED` flag to 0

    raise_error(); // print error traceback and call standart `exit()` function with `ERROR_OCCURED` code

    // if you need to add your custom text to traceback use `update_traceback`
    update_traceback("custom error text to add");

    // to print error and stop the program
    RAISE_ON_ERROR(some_action()) 

    /*
    to pass the previouse error if occured and start new traceback message
    "my_function_name" -- name of function, where error occured
    NIL_POINTER        -- error code (can use any of predescribed codes)
    */
    NEW_ERROR("my_function_name", NIL_POINTER)

    /*
    if you just want to continue traceback and say, it occured in "my_function_name"
    use CONTINUE_ERROR
    */
    CONTINUE_ERROR("my_function_name")

    /* to perform some action and make some actions if error occures use `TRY` macro */
    TRY(some_action(),
        CONTINUE_ERROR("my_function_name")
        return 0
    )

    /* to perform some actions if condition if true use `EXCEPT` macro */
    EXCEPT( some_value == 0,
        NEW_ERROR("my_function_name", NIL_POINTER)
        return 0    
    )

// ---------- element erasion ----------
    RAISE_ON_ERROR(array_erase(a, 1)); // delete element by index 1

// ---------- print array ----------
    RAISE_ON_ERROR(array_print(a)); printf("\n");
    // prints array like [123, 321.123. "asd"]

// ---------- array_free check ----------
    RAISE_ON_ERROR(array_free(a)); // free arrau

    return 0;
}
```