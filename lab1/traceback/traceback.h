#pragma once

#define ALLOC_ERROR_CODE 1
#define INDEX_OUT_OF_RANGE_CODE 2
#define NIL_POINTER_CODE 3
#define NOT_SUPPORTED_OPERATION_CODE 4
#define TYPE_ERROR_CODE 5

struct __stack_el;
struct err_stack;

extern struct err_stack g_traceback;
extern struct __stack_el **g_current_function;

char funcstack_get_flag();
char *funcstack_get_funcname();
void funcstack_push(char *funcname);
void funcstack_pop();
void funcstack_set_flag(char flag);

void traceback_start(char *text, int error_code);
void traceback_continue();
void traceback_add_message(char *msg);
void traceback_free();
void traceback_print();


#define ERROR_OCCURED (funcstack_get_flag())

#define ALLOC_ERROR (traceback_start("unable to allocate memory", ALLOC_ERROR_CODE))
#define INDEX_OUT_OF_RANGE (traceback_start("index out of range", INDEX_OUT_OF_RANGE_CODE))
#define NIL_POINTER (traceback_start("nil pointer exception", NIL_POINTER_CODE))
#define NOT_SUPPORTED_OPERATION (traceback_start("operation is not supported", NOT_SUPPORTED_OPERATION_CODE))
#define TYPE_ERROR (traceback_start("type error", TYPE_ERROR_CODE))

#define CONTINUE_ERROR (traceback_continue())

#define TRY(__expr, __on_error) { \
    funcstack_push(#__expr);\
    __expr; \
    if (ERROR_OCCURED) { \
        {char __code = funcstack_get_flag(); CONTINUE_ERROR; funcstack_pop(); funcstack_set_flag(__code);} \
        __on_error; \
    } else { funcstack_pop(); } \
}

#define EXCEPT(_condition, __expr) { \
    if (_condition) { \
        __expr; \
    } \
}

// #define RAISE_ON_ERROR(__expr) {__expr; if (ERROR_OCCURED) raise_error();} 
