#include "traceback.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MAX_ERROR_MSG_LENGTH 150
#define MAX_FUNCTION_NAME_LENGTH 140

struct __stack_el {
    char msg[MAX_ERROR_MSG_LENGTH];
    char flag;
    struct __stack_el *next;
};

struct err_stack {
    int err_code;
    struct __stack_el *head;
};

struct err_stack g_traceback = {};
struct __stack_el **g_current_function = NULL;

struct __stack_el *stackel_new(char *text) {
    struct __stack_el *new_el = (struct __stack_el *)malloc(sizeof(struct __stack_el));
    if (!new_el) {
        fprintf(stderr, "\n\nunable to allocate memory at all!\n");
        abort();
    }
    
    strncpy(new_el->msg, text, MAX_ERROR_MSG_LENGTH);
    new_el->next = NULL;
    new_el->flag = 0;

    return new_el;
}

void traceback_print() {
    puts("\033[0;31m\n");
    for (struct __stack_el *it = g_traceback.head; it; it = it->next) {
        puts(it->msg);
    }

    printf("error code: %d\033[0m\n\n", g_traceback.err_code);
}

void traceback_free() {
    struct __stack_el *it = g_traceback.head;
    while (it) {
        struct __stack_el *tmp = it->next;
        free(it);
        it = tmp;
    }
    g_traceback.head = NULL;
    g_traceback.err_code = 0;
    funcstack_set_flag(0);
}

void traceback_add_message(char *msg) {
    struct __stack_el *new_el = stackel_new(msg);
    new_el->next = g_traceback.head;

    g_traceback.head = new_el;
}

void traceback_start(char *text, int error_code) {
    if (g_traceback.head) {
        traceback_add_message("\nduring handling of the above exception, another exception has occured!\n");
    }
    char tmp[MAX_ERROR_MSG_LENGTH];
    snprintf(tmp, MAX_ERROR_MSG_LENGTH, "an error occured: `%s`", text);
    traceback_add_message(tmp);
    snprintf(tmp, MAX_ERROR_MSG_LENGTH, "here: `%s`", funcstack_get_funcname());
    traceback_add_message(tmp);

    funcstack_set_flag(error_code);
    g_traceback.err_code = error_code;
}

void traceback_continue() {
    char tmp[MAX_ERROR_MSG_LENGTH];
    snprintf(tmp, MAX_ERROR_MSG_LENGTH, "stacked from here: `%s`", funcstack_get_funcname());
    traceback_add_message(tmp);
}

void funcstack_push(char *funcname) {
    struct __stack_el *new_el = stackel_new(funcname);
    new_el->next = *g_current_function;
    *g_current_function = new_el;
}

void funcstack_pop() {
    if (!*g_current_function) return;

    struct __stack_el *tmp = (*g_current_function)->next;
    free(*g_current_function);
    *g_current_function = tmp;
}

char funcstack_get_flag() {
    return (*g_current_function)->flag;
}

void funcstack_set_flag(char flag) {
    (*g_current_function)->flag = flag;
}

char *funcstack_get_funcname() {
    return (*g_current_function)->msg;
}

__attribute__((constructor))
void __program_start_handler() {
    g_current_function = (struct __stack_el **)calloc(1, sizeof(struct __stack_el *));
    funcstack_push("main(argc, argv)");
}

__attribute__ ((destructor))
void __program_end_handler() {
    if (ERROR_OCCURED) {
        traceback_print();
        traceback_free();
    }
    funcstack_pop();
    free(g_current_function);
}
