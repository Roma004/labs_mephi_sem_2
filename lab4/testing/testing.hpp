#pragma once

#include <functional>
#include <iostream>
#include <map>
#include <string>


class Testing {
private:
    using cstr = const char *;

    typedef std::function<void ()> test;
    typedef std::map<cstr, test> subcategory;
    typedef std::map<cstr, subcategory> category;

    std::map<cstr, category> ctgs;

    bool process_test(cstr category_name, cstr subcategory_name, cstr name);
    int process_subcategory(cstr category_name, cstr name);
    std::pair<int, int> process_category(cstr name);

public:

    void add_category(cstr name);
    void add_category(cstr name, category subctgs);
    void add_subcategory(cstr category_name, cstr name);
    void add_subcategory(cstr category_name, cstr name, subcategory t);
    void add_test(cstr category_name, cstr subcategory_name, cstr name, test t);

    void process();
};
