#include "testing.hpp"

bool Testing::process_test(cstr category_name, cstr subcategory_name, cstr name)  {
    try {
        std::cout << "    \033[1;37m" << name << "\033[0m: " << std::flush;
        ctgs[category_name][subcategory_name][name]();
        std::cout << "\033[0;32mOK\033[0m\n";
        return true;
    } catch (std::string error) {
        std::cout << "\033[0;31mFAILURE\033[0m\n";
        std::cout << error << "\n";
        return false;
    }
}

int Testing::process_subcategory(cstr category_name, cstr name) {
    std::cout << "  \033[1;35m" << name << "\033[0m\n" << std::flush;

    int test_counter = 0;
    for (auto &&[aim, process]: ctgs[category_name][name]) {
        test_counter += process_test(category_name, name, aim);
    }

    return test_counter;
}

std::pair<int, int> Testing::process_category(cstr name) {
    std::cout << "\033[1;34m\n" << name << " tests\033[0m\n" << std::flush;
    
    int test_counter = 0;
    int total = 0;
    for (auto &&[aim, subctg]: ctgs[name]) {
        test_counter += process_subcategory(name, aim);
        total += subctg.size();
    }

    std::cout << "\033[1;37m---------------------------------\033[0m\n";
    if (test_counter != total) {
        std::cout << "\033[0;31m" << test_counter << "\033[0m out of \033[0;32m" << total << "\033[0m tests passed!\n";
    } else {
        std::cout << "\033[0;32mAll tests in category `" << name << "` passed!\033[0m\n";
    }

    return {test_counter, total};
}

void Testing::add_category(cstr name) {
    ctgs[name] = {};
}

void Testing::add_category(cstr name, category subctgs) {
    ctgs[name] = subctgs;
}

void Testing::add_subcategory(cstr category_name, cstr name) {
    ctgs[category_name][name] = {};
}

void Testing::add_subcategory(cstr category_name, cstr name, subcategory t) {
    ctgs[category_name][name] = t;
}

void Testing::add_test(cstr category_name, cstr subcategory_name, cstr name, test t) {
    ctgs[category_name][subcategory_name][name] = t;
}

void Testing::process() {
    std::cout << "\033[1;36mTesting started!\033[0m\n";

    std::map<cstr, std::pair<int, int>> report;
    for (auto &&[name, category]: ctgs) report[name] = {0, 0};

    for (auto &&[name, category]: ctgs) {
        auto res = process_category(name);;
        report[name].first += res.first;
        report[name].second += res.second;
    }

    std::cout << "\n\033[1;36mTests summary:\033[0m\n";
    for (auto &&[name, passed]: report) {
        std::cout << "\033[1;34m"
            << name << ": "
            << (passed.first == passed.second ? "\033[0;32m" : "\033[0;31m")
            << passed.first << "\033[0m/\033[0;32m" << passed.second << "\033[0m\n";
    }
}