#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <ostream>
#include <ratio>
#include <sstream>
#include <stdexcept>
#include <string>
#include <iostream>
#include <chrono>

#include "include/Heap.hpp"

using string = std::string;
using timestamp = std::chrono::high_resolution_clock::time_point;

string get_str(const char *prompt) {
    printf("%s", prompt);
    string str;
    std::getline(std::cin, str);

    return str;
}

int get_int(const char *prompt) {
    std::stringstream tmp_input;
    tmp_input << get_str(prompt);

    int res; tmp_input >> res;
    return res;
}

int *get_random_array(int length) {
    int *res = new int[length];
    for (int i = 0; i < length; ++i) {
        res[i] = rand();
    }
    return res;
}

void run_insertion(int *data, int length, int cycles) {
    int append_data;

    std::chrono::duration<double, std::milli> ins_time;
    std::chrono::duration<double, std::milli> del_time;

    Heap<int> l(data, length);

    std::cout << "\nInsertion speed: 0ms.| Removal speed: 0ms." << std::flush;
    for (int i = 1; i <= cycles; ++i) {
        append_data = rand();

        timestamp t1 = std::chrono::high_resolution_clock::now();
        l.insert(append_data);
        ins_time += std::chrono::high_resolution_clock::now() - t1;

        t1 = std::chrono::high_resolution_clock::now();
        l.remove(append_data);
        del_time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rInsertion speed: \033[1;37m" << ins_time.count() / i <<  "ms.\033[0m | " 
                  << "Removal speed: \033[1;37m" << del_time.count() / i <<  "ms.\033[0m                " << std::flush;
    }

    puts("");
}

void run_search(int *data, int length, int cycles) {
    std::chrono::duration<double, std::milli> time;
    std::chrono::duration<double, std::milli> slice_time;

    std::cout << "\nSearch speed: 0ms." << std::flush;
    Heap<int> l(data, length);
    Heap<int>::iterator it = l.begin();
    for (int i = 1; i <= cycles; ++i) {

        timestamp t1 = std::chrono::high_resolution_clock::now();
        it = l.find(rand());
        slice_time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rSearch speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m      " << std::flush;
    }

    puts("");
}


void run_concatenation(int *data, int length, int cycles) {
    std::chrono::duration<double, std::milli> time;

    std::cout << "\nConcatenation speed: 0ms." << std::flush;
    Heap<int> l(data, length), s(data, length), r;
    for (int i = 1; i <= cycles; ++i) {

        timestamp t1 = std::chrono::high_resolution_clock::now();
        r = l + s;
        time += std::chrono::high_resolution_clock::now() - t1;

        std::cout << "\rConcatenation speed: \033[1;37m" << time.count() / i <<  "ms.\033[0m          " << std::flush;
    }
}


void search_timing(int *data, int length, int cycles) {
    puts("\n\033[0;32mStarting range deletion timing tests\033[0m");
    run_search(data, length, cycles);
}

void insertion_timing(int *data, int length, int cycles) {
    puts("\n\033[0;32mStarting insertion/deletion timing tests\033[0m");
    run_insertion(data, length, cycles);
}

void concatenation_timing(int *data, int length, int cycles) {
    puts("\n\033[0;32mStarting merging timing tests\033[0m");
    run_concatenation(data, length, cycles);
}

void set_choice(Heap<int> &arr) {
    int choice = get_int(
        "Select print order (RoLR - default):\n"
        "  1) RoLR\n"
        "  2) RoRL\n"
        "  3) LRRo\n"
        "  4) LRoR\n"
        "  5) RLRo\n"
        "  6) RRoL\n\n"
        "Your choice: "
    );
    switch (choice) {
    case 1: arr.set_search_order(RoLR); break;
    case 2: arr.set_search_order(RoRL); break;
    case 3: arr.set_search_order(LRRo); break;
    case 4: arr.set_search_order(LRoR); break;
    case 5: arr.set_search_order(RLRo); break;
    case 6: arr.set_search_order(RRoL); break;
    default: arr.set_search_order(RoLR); break;
    }
}

int main() {
    char res = 0;

    Heap<int> arr;
    Heap<int> tmp;
    int start, end;
    int length;
    int val;
    int choice;
    int cycles;
    int *data;
    std::string s;
    std::stringstream ss;

    while (!res && !std::cin.eof()) {
        string input = get_str("\nCommand (`h` for help): ");
        switch (input[0]) {
        case 'h':
            printf(
                "Available options:\n"
                "\n"
                "  General:\n"
                "   h -- print this menue\n"
                "   q -- stop this program \n"
                "\n"
                "  Heap:\n"
                "   i -- insert new element\n"
                "   p -- print in selected order\n"
                "   r -- read from keyboard\n"
                "   d -- delete element\n"
                "   s -- print subtree\n"
                "   m -- merge two heaps \n"
                "   c -- clear heap \n"
                "   t -- start timing tests \n"
            );
            break;

        case 'q':
            res = 1;
            break;

        case 'i':
            val = get_int("Enter new element value: ");

            try {
                arr.insert(val);
            } catch (std::exception &e) {
                puts(e.what());
                break;
            }

            puts("\nElement was successfully inserted!");
            break;

        case 'p':
            set_choice(arr);
            for (auto &it: arr) std::cout << it << " ";
            std::cout << "\n\n";

            break;

        case 'r':
            arr.clear();
            ss.clear();

            puts("enter the sequence: ");
            std::getline(std::cin, s); ss << s;
            // get_str("");
            int el;

            while (ss >> el) arr.insert(el);

            puts("\nElements were successfully read!");
            break;
            
        case 'd':

            val = get_int("Enter value to delete: ");
        
            try {
                arr.remove(val);
            } catch (std::runtime_error &e) {
                puts(e.what());
                break;
            }

            puts("\nElement was successfully deleted!");
            break;

        case 's':
            val = get_int("Enter subtree root value: ");
            set_choice(arr);

            for (auto &it: arr.subtree(val)) std::cout << it << " ";
            std::cout << "\n\n";
            break;

        case 'm':
            ss.clear();
            puts("enter the sequence: ");
            std::getline(std::cin, s); ss << s;
            tmp.clear();

            while (ss >> el) tmp.insert(el);
            tmp += arr;
            set_choice(tmp);

            for (auto &it: tmp) std::cout << it << " ";

            input = get_str("\nDo you want to save the result [Y/n]?: ");
            if (input[0] == 'Y') arr = tmp;

            puts("OK!");
            break;

        case 't':

            do length = get_int("Enter the length of sequence: "); while (length < 0);
            do cycles = get_int("Enter the number of test cycles: "); while (length < 0);

            srand(time(NULL));
            data = get_random_array(length);
            insertion_timing(data, length, cycles);
            search_timing(data, length, cycles);
            concatenation_timing(data, length, cycles);

            delete [] data;
            break;

        default:
            printf("unknown command `%c`. Enter `m` to get help\n", input[0]);

            break;
        }
    }

    return res;
}