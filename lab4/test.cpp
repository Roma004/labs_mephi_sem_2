#include <algorithm>
#include <exception>
#include <iostream>
#include <ostream>
#include <sstream>
#include <stdexcept>
#include <functional>
#include <map> 
#include <set>
#include <vector>
#include "include/Heap.hpp"
#include "testing/testing.hpp"

/*
Heap view:
`          0  
`     1         2
`  3     4   5     6
` 7 8   9 

`                       0   
`           0                       1 
`     3           1            2          3 
`  5     7     9     4      5     2    6     4
` 7 6   8 8   9

`           a
`     b           c
`  d     e     f     g
` h

orders:
    RoLR = 123,
    RoRL = 132,
    LRRo = 312,
    LRoR = 213,
    RLRo = 321,
    RRoL = 231
*/

// iteration
void iterate_over_heap(Heap<int> &a, const HeapSearchOrders order, const std::vector<int> correct);
void iterate_over_subtree(
    Heap<int> &a,
    const Heap<int>::iterator start,
    const Heap<int>::iterator end,
    const HeapSearchOrders order, 
    const std::vector<int> correct
);

void search(Heap<int> &a, const std::vector<int> vals, bool exists);
void search_after_iterator(Heap<int> &a, const Heap<int>::iterator iter, const std::vector<int> vals, bool exists);
void search_between_iterators(
    Heap<int> &a,
    const Heap<int>::iterator start,
    const Heap<int>::iterator end,
    const std::vector<int> vals, bool exists
);
void insertion(Heap<int> &a, const std::vector<int> vals);
void fake_removal(Heap<int> &a, const std::vector<int> vals);
void removal(Heap<int> &a, const std::vector<int> vals);
void clearing(Heap<int> &a);

void mapping(Heap<int> &a, std::function<int (int)> func, std::multiset<int> correct);
void whereing(Heap<int> &a, std::function<bool (int)> func, std::multiset<int> correct);
void concatenating(Heap<int> &a, Heap<int> b, std::multiset<int> correct);
void getting_subtree(Heap<int> &a, int elem, std::multiset<int> correct);

// TODO conversion
void serializing(Heap<int> &a, std::string format, std::string correct);

int make_negative(int a) { return -a; }
int is_odd(int a) { return a % 2 == 1; }

int main() {
    Heap<int> a{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    Heap<int> a_long{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    Heap<int> b;

    Heap<int> sub_1 = a.get_subtree(1);
    Heap<int> sub_2 = a.get_subtree(2);
    Heap<int> sub_4 = a.get_subtree(4);
    Heap<int> sub_7 = a.get_subtree(7);

    const Heap<int>::iterator iter_1 = a.find(1);
    const Heap<int>::iterator iter_8 = a.find(8);

    Heap<std::string> str_a{"a", "b", "c", "d", "e", "f", "g", "h"};

    /*
    `                       0   
    `           0                       1 
    `     3           1            2          3 
    `  5     7     9     4      5     2    6     4
    ` 7 6   8 8   9
    */

    for (auto &it: a_long.subtree(1)) {
        std::cout << it << " "; 
    }

    // Heap<int> asd = a + a;
    // for (auto it = str_a.get_data(); it != str_a.get_data()+str_a.lenght(); ++it) std::cout << *it << " ";
    // std::cout << "\n";

    std::cout 
        << "Test heap tree view:\n"
        << "         0\n"
        << "    1         2\n"
        << " 3     4   5     6\n"
        << "7 8   9 \n";

    Testing t;

    // t.add_category("Debug", {
    //     {"debug", {
    //         // {"not existing values", [&](){ search(a, {-1, 10, 123, 1}, false); }}
    //         // {"RoRL order iteration", [&](){ iterate_over_heap(a, RoRL, {0, 2, 6, 5, 1, 4, 9, 3, 8, 7}); }},
    //         // {"LRoR order iteration", [&](){ iterate_over_heap(a_long, LRoR, {7, 5, 6, 3, 8, 7, 8, 0, 9, 9, 1, 4, 0, 5, 2, 2, 1, 6, 3, 4}); }},
    //     }}
    // });

    t.add_category("Basic", {
        {"search", {
            {"existing values", [&](){ search(a, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, true); }},
            {"not existing values", [&](){ search(a, {-1, 10, 123}, false); }},
        }},

        {"itreator search", {
            {"existing values after element 1", [&](){ search_after_iterator(a, iter_1, {1, 3, 4, 7, 8, 9}, true); }},
            {"not existing values element 1", [&](){ search_after_iterator(a, iter_1, {-1, 10, 123, 0, 5, 6, 2}, false); }},
            {"existing values between 1 and 8", [&](){ search_between_iterators(a, iter_1, iter_8, {1, 3, 4, 7, 8}, true); }},
            {"not existing values between 1 and 8", [&](){ search_between_iterators(a, iter_1, iter_8, {-1, 10, 123, 0, 5, 6, 2, 9, 4}, false); }},
            {"begin is greater then end", [&](){ search_between_iterators(a, iter_8, iter_1, {}, true); }}
        }},

        {"insertion", {
            {"insert values", [&](){ insertion(b, {123, 234, 456, 1, 2, 6, 3, -5}); }}
        }},

        {"removal", {
            {"existing values", [&](){ removal(b, {123, 2, 456, -5}); }},
            {"not existing values", [&](){ fake_removal(b, {1234, -1, 8}); }},
            {"clearing", [&](){ clearing(b); }},
        }}
    });

    // Iteration over heap and its' subtrees
    t.add_category("Iteration", {
        /*
        `          0  
        `     1         2
        `  3     4   5     6
        ` 7 8   9 
        */
        {"heap of 10 elements", {
            {"RoLR order iteration", [&](){ iterate_over_heap(a, RoLR, {0, 1, 3, 7, 8, 4, 9, 2, 5, 6}); }},
            {"RoRL order iteration", [&](){ iterate_over_heap(a, RoRL, {0, 2, 6, 5, 1, 4, 9, 3, 8, 7}); }},
            {"LRRo order iteration", [&](){ iterate_over_heap(a, LRRo, {7, 8, 3, 9, 4, 1, 5, 6, 2, 0}); }},
            {"LRoR order iteration", [&](){ iterate_over_heap(a, LRoR, {7, 3, 8, 1, 9, 4, 0, 5, 2, 6}); }},
            {"RLRo order iteration", [&](){ iterate_over_heap(a, RLRo, {6, 5, 2, 9, 4, 8, 7, 3, 1, 0}); }},
            {"RRoL order iteration", [&](){ iterate_over_heap(a, RRoL, {6, 2, 5, 0, 4, 9, 1, 8, 3, 7}); }}
        }},

        /*
        `                       0   
        `           0                       1 
        `     3           1            2          3 
        `  5     7     9     4      5     2    6     4
        ` 7 6   8 8   9
        */
        {"heap of 20 elements", {
            {"RoLR order iteration", [&](){ iterate_over_heap(a_long, RoLR, {0, 0, 3, 5, 7, 6, 7, 8, 8, 1, 9, 9, 4, 1, 2, 5, 2, 3, 6, 4}); }},
            {"RoRL order iteration", [&](){ iterate_over_heap(a_long, RoRL, {0, 1, 3, 4, 6, 2, 2, 5, 0, 1, 4, 9, 9, 3, 7, 8, 8, 5, 6, 7}); }},
            {"LRRo order iteration", [&](){ iterate_over_heap(a_long, LRRo, {7, 6, 5, 8, 8, 7, 3, 9, 9, 4, 1, 0, 5, 2, 2, 6, 4, 3, 1, 0}); }},
            {"LRoR order iteration", [&](){ iterate_over_heap(a_long, LRoR, {7, 5, 6, 3, 8, 7, 8, 0, 9, 9, 1, 4, 0, 5, 2, 2, 1, 6, 3, 4}); }},
            {"RLRo order iteration", [&](){ iterate_over_heap(a_long, RLRo, {4, 6, 3, 2, 5, 2, 1, 4, 9, 9, 1, 8, 8, 7, 6, 7, 5, 3, 0, 0}); }},
            {"RRoL order iteration", [&](){ iterate_over_heap(a_long, RRoL, {4, 3, 6, 1, 2, 2, 5, 0, 4, 1, 9, 9, 0, 8, 7, 8, 3, 6, 5, 7}); }}
        }},

        {"subtree of element 1", {
            {"RoLR order iteration", [&](){ iterate_over_heap(sub_1, RoLR, {1, 3, 8, 4, 7, 9}); }},
            {"RoRL order iteration", [&](){ iterate_over_heap(sub_1, RoRL, {1, 7, 9, 3, 4, 8}); }},
            {"LRRo order iteration", [&](){ iterate_over_heap(sub_1, LRRo, {8, 4, 3, 9, 7, 1}); }},
            {"LRoR order iteration", [&](){ iterate_over_heap(sub_1, LRoR, {8, 3, 4, 1, 9, 7}); }},
            {"RLRo order iteration", [&](){ iterate_over_heap(sub_1, RLRo, {9, 7, 4, 8, 3, 1}); }},
            {"RRoL order iteration", [&](){ iterate_over_heap(sub_1, RRoL, {7, 9, 1, 4, 3, 8}); }}
        }},

        {"subtree of element 2", {
            {"RoLR order iteration", [&](){ iterate_over_heap(sub_2, RoLR, {2, 5, 6}); }},
            {"RoRL order iteration", [&](){ iterate_over_heap(sub_2, RoRL, {2, 6, 5}); }},
            {"LRRo order iteration", [&](){ iterate_over_heap(sub_2, LRRo, {5, 6, 2}); }},
            {"LRoR order iteration", [&](){ iterate_over_heap(sub_2, LRoR, {5, 2, 6}); }},
            {"RLRo order iteration", [&](){ iterate_over_heap(sub_2, RLRo, {6, 5, 2}); }},
            {"RRoL order iteration", [&](){ iterate_over_heap(sub_2, RRoL, {6, 2, 5}); }}
        }},

        {"subtree of element 4", {
            {"RoLR order iteration", [&](){ iterate_over_heap(sub_4, RoLR, {4, 9}); }},
            {"RoRL order iteration", [&](){ iterate_over_heap(sub_4, RoRL, {4, 9}); }},
            {"LRRo order iteration", [&](){ iterate_over_heap(sub_4, LRRo, {9, 4}); }},
            {"LRoR order iteration", [&](){ iterate_over_heap(sub_4, LRoR, {9, 4}); }},
            {"RLRo order iteration", [&](){ iterate_over_heap(sub_4, RLRo, {9, 4}); }},
            {"RRoL order iteration", [&](){ iterate_over_heap(sub_4, RRoL, {4, 9}); }}
        }},

        {"subtree of element 7", {
            {"RoLR order iteration", [&](){ iterate_over_heap(sub_7, RoLR, {7}); }},
            {"RoRL order iteration", [&](){ iterate_over_heap(sub_7, RoRL, {7}); }},
            {"LRRo order iteration", [&](){ iterate_over_heap(sub_7, LRRo, {7}); }},
            {"LRoR order iteration", [&](){ iterate_over_heap(sub_7, LRoR, {7}); }},
            {"RLRo order iteration", [&](){ iterate_over_heap(sub_7, RLRo, {7}); }},
            {"RRoL order iteration", [&](){ iterate_over_heap(sub_7, RRoL, {7}); }}
        }}
    });

    t.add_category("Conversion", {
        {"mapping", {
            {"make negative", [&](){ mapping(a, make_negative, {0, -1, -2, -3, -4, -5, -6, -7, -8, -9}); }}
        }},

        {"whereing", {
            {"less than 3", [&](){ whereing(a, is_odd, {1, 3, 5, 7, 9}); }}
        }},

        {"merging", {
            {"simple merging", [&](){ concatenating(a, a, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9}); }},
            {"merging heap with null", [&](){ concatenating(a, b, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}); }},
            {"merging null with heap", [&](){ concatenating(b, a, {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}); }},
            {"merging null with null", [&](){ concatenating(b, b, {}); }},
        }},

        {"string serializing", {
            {"format {L}--{Ro}--{R}", [&](){ serializing(
                a, "({L}--{Ro}--{R})",
                "((((--7--)--3--(--8--))--1--((--9--)--4--))--0--((--5--)--2--(--6--)))"
            ); }},
            {"format (left branch): ({L}--{Ro})", [&](){ serializing(a, "({L}--{Ro})", "((((--7)--3)--1)--0)"); }},
            {"format (right branch): ({R}--{Ro})", [&](){ serializing(a, "({R}--{Ro})", "(((--6)--2)--0)"); }},
            {"format (root-only): (--{Ro}--)", [&](){ serializing(a, "(--{Ro}--)", "(--0--)"); }},
            {"format null <format: ({L}--{Ro}--{R})>", [&](){ serializing(b, "({L}--{Ro}--{R})", "(----)"); }},
        }}
    });

    t.process();

    return 0;
}

///////////////////////////////// BASIC TESTS /////////////////////////////////

#define search_body(find_expression) { \
    bool flag = false; \
    std::vector<int> bad_values; \
    for (auto &it: vals) { \
        if (((find_expression) != a.end()) != exists) bad_values.push_back(it); \
    } \
    if (flag) { \
        std::stringstream ss; \
        ss << "Checking values existance failes:\nvalues:  "; \
        for (auto &it: bad_values) ss << "`" << it << "` "; \
        if (exists) ss << " could not be found, however exists!\n"; \
        else        ss << " was found, however does not exist!\n"; \
        throw ss.str(); \
    } \
}

#define insert_remove_body(try_clause, except_clause, check_clause) { \
    std::stringstream ss; \
    for (auto &it: vals) { \
        try { try_clause } catch (std::runtime_error err) { except_clause } \
    } \
    try { check_clause } catch (std::string err) { \
        ss << err; \
        throw ss.str(); \
    } \
}

void search(Heap<int> &a, const std::vector<int> vals, bool exists) 
    search_body(a.find(it))

void search_after_iterator(Heap<int> &a, const Heap<int>::iterator iter, const std::vector<int> vals, bool exists) 
    search_body(a.find(iter, a.end(), it))

void search_between_iterators(
    Heap<int> &a,
    const Heap<int>::iterator start,
    const Heap<int>::iterator end,
    const std::vector<int> vals, bool exists
) search_body(a.find(start, end, it))

void insertion(Heap<int> &a, const std::vector<int> vals) 
    insert_remove_body(
        a.insert(it);,
        ss << "An error occured during value `" << it << "` insertion: `" << err.what() << "`\n";,
        search(a, vals, true);
    )

void removal(Heap<int> &a, const std::vector<int> vals)
    insert_remove_body(
        a.remove(it);,
        ss << "An error occured during value `" << it << "` removal: `" << err.what() << "`\n";,
        search(a, vals, false);
    )

void fake_removal(Heap<int> &a, const std::vector<int> vals) 
    insert_remove_body(
        a.remove(it);
        ss << "removal of unexisting value `" << it << "` not fails!\n";,
        /* everything is ok! */,
        search(a, vals, false);
    )

void clearing(Heap<int> &a) {
    try { a.clear(); } catch (std::runtime_error err) { 
        throw err.what();
    }
}

////////////////////////////// TESTS FOR ITERATOR /////////////////////////////

void iterate_over_heap(Heap<int> &a, HeapSearchOrders order, const std::vector<int> correct) {
    a.set_search_order(order);
    iterate_over_subtree(a, a.begin(), a.end(), order, correct);
}

void iterate_over_subtree(
    Heap<int> &a,
    Heap<int>::iterator start,
    Heap<int>::iterator end,
    HeapSearchOrders order, 
    const std::vector<int> correct
) {
    a.set_search_order(order);

    auto ans_it = correct.begin();
    std::stringstream ss;
    int idx = 0;
    ss << "interation result: ";

    bool flag = false;
    bool print_size = true;
    for (auto it = start; it != end; ++it) {
        ss << *it << " ";
        if (
            idx >= correct.size() || 
            (idx < correct.size() && *ans_it != *it)
        ) flag = true;
        
        ++idx;
        ++ans_it;

        if (idx > correct.size() * 3) {
            ss << "interrupting...\nsequence is more than 3 times longer than estimated\n";
            print_size = false;
            break;
        }
    }
    if (print_size) ss << "\nsize: " << idx << "\n";
    
    ss << "correct answer was: ";
    for (auto &it: correct) ss << it << " ";

    ss << "\nsize: " << correct.size() << "\n";

    if (flag) throw std::string(ss.str());
}

////////////////////////////// CONVERSION TESTS ///////////////////////////////

#define conversion_body(action) { \
    std::stringstream ss; \
    Heap<int> res; \
    try {  \
        res = action;  \
    } catch (std::runtime_error err) { \
        ss << "Error during mapping a: `" << err.what() << "`\n"; \
    } \
    std::vector<int> not_found; \
    int corr_size = correct.size(); \
    for (auto &it: res) { \
        if (correct.find(it) == correct.end()) not_found.push_back(it); \
        else correct.erase(correct.equal_range(it).first); \
        if (not_found.size() > corr_size*3) { \
            ss << "interrupting comparsion! 3 times more iterations than alleged!\n"; \
            break; \
        } \
    } \
    if (!not_found.empty()) { \
        ss << "Values: "; \
        for (auto &it: not_found) ss << it << " "; \
        ss << "are excess\n"; \
    } \
    if (!correct.empty()) { \
        ss << "Values:  "; \
        for (auto &it: correct) ss << it << " "; \
        ss << "  disappeared\n"; \
    } \
    if (!ss.str().empty()) throw ss.str(); \
}

void mapping(Heap<int> &a, std::function<int (int)> func, std::multiset<int> correct) conversion_body(a.map(func))
void whereing(Heap<int> &a, std::function<bool (int)> func, std::multiset<int> correct) conversion_body(a.where(func))
void concatenating(Heap<int> &a, Heap<int> b, std::multiset<int> correct) conversion_body(a + b)
void getting_subtree(Heap<int> &a, int elem, std::multiset<int> correct) conversion_body(a.get_subtree(elem))
void serializing(Heap<int> &a, std::string format, std::string correct) {
    std::stringstream ss;
    std::string res;
    try { res = a.serialize(format); } catch (std::exception &err) {
        ss << "During heap serialization exception occured: " << err.what() << "\n";
        throw ss.str();
    }

    if (res != correct) {
        ss << "result value is incorrect!\nreceived: `" << res << "`\nalleged:  `" << correct << "`\n";
        throw ss.str();
    }
}
