#pragma once

#include <algorithm>
#include <cstddef>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <cmath>
#include <cstring>

enum HeapSearchOrders {
    RoLR = 123,
    RoRL = 132,
    LRRo = 312,
    LRoR = 213,
    RLRo = 321,
    RRoL = 231
};

template<class T>
class Heap {
public:
    class iterator;
    friend class iterator;
    class heap_subtree {
    private:
        Heap *parrent;
        size_t idx;

        heap_subtree(Heap *parrent, size_t idx): parrent(parrent), idx(idx) {}
        friend class Heap;
    public:
        Heap::iterator begin() const;
        Heap::iterator end() const;
    };
    friend class heap_subtree;

private:
    struct search_order_t {
        char root;
        char left;
        char right;
        search_order_t(HeapSearchOrders num):
            root(num / 100),
            left(num % 100 / 10),
            right(num % 10) {}
    };

    T *data;
    size_t memsize;
    size_t size;
    search_order_t order;

    Heap(size_t size): data(new T[size]), memsize(size), size(0), order(RoLR) {}

    void resize();
    void shift_up(size_t ind);
    void shift_down(size_t ind);

    using string = std::string;

    std::string serialise_subtree(size_t idx, std::function<string (string, string, string)>);

public:
    Heap(): Heap(10) {}
    
    Heap(T *elems, size_t num): Heap(num*2) {
        for (size_t i = 0; i < num; ++i) insert(elems[i]);
    }

    Heap(std::initializer_list<T> list): Heap(list.size()*2) {
        for (auto &i : list) insert(i);
    }

    Heap(const Heap &h): order(h.order), size(h.size), memsize(h.memsize) {
        data = new T[memsize];
        std::copy(h.data, h.data+size, data);
    }

    Heap(const Heap &&h): 
        order(h.order), size(h.size),
        memsize(h.memsize), data(std::move(h.data)) {}

    ~Heap() {
        delete [] data;
    }

    Heap &operator+=(const Heap &other) {
        for (int i = 0; i < other.size; ++i) insert(other.data[i]);
        return *this;
    }

    Heap operator+(const Heap &other) {
        Heap res(*this);
        res += other;
        return res;
    }

    Heap &operator=(const Heap &h) {
        delete [] data;
        order = h.order; size = h.size, memsize = h.memsize;
        data = new T[size];
        std::copy(h.data, h.data+size, data);
        return *this;
    }

    Heap &operator=(Heap &&h) {
        delete [] data;
        order = h.order; size = h.size, memsize = h.memsize;
        data = std::move(h.data);
        h.data = nullptr;
        return *this;
    }

    void set_search_order(HeapSearchOrders new_order) {
        order = search_order_t(new_order);
    }

    void clear() {
        memset(data, 0, memsize*sizeof(T));
        size = 0;
    }

    iterator find(T elem) const;
    iterator find(const iterator from, const iterator to, T elem) const;

    heap_subtree subtree(T elem);
    heap_subtree subtree(iterator elem_iter);

    Heap get_subtree(T elem);
    Heap map(std::function<T (T)>);
    Heap where(std::function<bool (T)>);

    iterator begin() const;
    iterator end() const;

    void insert(T elem);
    void remove(T elem);

    T head() { return data[0]; }
    size_t lenght() { return size; }

    std::string serialize(std::string format);
};

//////////////////////////////// HEAP ITERATOR ////////////////////////////////

template<class T>
class Heap<T>::iterator: public std::iterator<std::input_iterator_tag, T> {
private:
    friend class Heap;
    T *data = nullptr;
    size_t size;
    size_t start;
    size_t idx;
    search_order_t order;

    #define left_idx (idx*2+1)
    #define right_idx (idx*2+2)
    #define parallel_idx (is_left() ? idx+1 : idx-1)
    #define left_is_major (order.left < order.right)
    #define right_is_major (!left_is_major)
    #define is_major (idx != start && (is_left() && left_is_major || !is_left() && right_is_major))

    #define go_major() { \
        if (left_is_major) idx = left_idx; \
        else idx = right_idx; \
    }
    #define go_minor() { \
        if (left_is_major) idx = right_idx; \
        else idx = left_idx; \
    }

    #define go_higher() { idx = (idx-1) / 2; }

    #define major_available ( \
        (left_is_major && left_idx < size) || \
        (!left_is_major && right_idx < size) \
    )

    #define minor_available ( \
        (!right_is_major && right_idx < size) || \
        (!left_is_major && left_idx < size) \
    )
    #define parallel_available (parallel_idx < size)

    #define switch_branch() { \
        if (is_left()) ++idx; \
        else --idx; \
    }

    iterator(T *data, size_t size, size_t start, size_t ind, search_order_t order): 
        data(data), size(size), idx(ind), order(order), start(start)
    {
        if (order.root != 1 && idx == start) {
            while (major_available) 
                go_major();

            if (order.root != 2) while (minor_available)
                go_minor();
        }    
    }

    iterator(T *data, size_t size, size_t ind, search_order_t order):
        iterator(data, size, 0, ind, order) {}

public:
    iterator(const iterator &it): 
        data(it.data), size(it.size), idx(it.idx), order(it.order), start(it.start) {}

    bool operator!=(iterator const& other) const { return data+idx != other.data+other.idx; }
    bool operator==(iterator const& other) const { return data+idx == other.data+other.idx; }
    typename iterator::reference operator*() const { return data[idx]; }
    iterator& operator++();
    // iterator& operator--();

    bool is_left() { return idx % 2 == 1; }
    size_t get_idx() { return idx; }
    void set_order(HeapSearchOrders order) {
        this->order = search_order_t(order);
    }
};

///////////////////////////// HEAP ITERATOR IMPLS /////////////////////////////

/*
`                       0   
`           0                       1 
`     3           1            2          3 
`  5     7     9     4      5     2    6     4
` 7 6   8 8   9
*/

template<class T>
typename Heap<T>::iterator &Heap<T>::iterator::operator++() { 
    if (idx == size) return *this;

    if (order.root == 1) {
        if (major_available) { go_major(); return *this; }
        if (minor_available) { go_minor(); return *this; }
        while (idx != start) {
            if (is_major && parallel_available) { switch_branch(); return *this; }
            go_higher();
        }
        
    } else if (order.root == 2) { 
        // return root after major branch
        if (left_idx >= size && is_major) { go_higher(); return *this; }

        // return minor after root
        if (minor_available) {
            go_minor(); while (major_available) go_major();
            return *this;
        }
        
        // return preceding root
        if (!is_major && idx != start) {
            while (!is_major && idx != start) go_higher();
            if (idx != start) { 
                go_higher();
                return *this; 
            } 
        }

    } else {  // order.root == 3
        // return minor after major branch
        if (idx == start) { idx = size; return *this; }

        if (is_major) { 
            switch_branch();
            while (major_available) go_major();
            if (minor_available) go_minor();
            
        } else go_higher();

        return *this; 
    }

    idx = size;

    return *this; 
}

#undef left_idx
#undef right_idx
#undef parallel_idx
#undef left_is_major
#undef right_is_major
#undef is_major
#undef go_major
#undef go_minor
#undef go_higher
#undef major_available
#undef minor_available
#undef parallel_available
#undef switch_branch

///////////////////////////// HEAP PRIVATE IMPLS //////////////////////////////

template<class T>
void Heap<T>::resize()  {
    memsize *= 2;
    T *tmp = new T[memsize];

    std::copy(data, data+size, tmp);
    delete [] data;

    data = tmp;
}

template<class T>
void Heap<T>::shift_up(size_t ind) {
    while (ind != 0 && data[ind] < data[(ind-1) / 2]) {
        std::swap(data[ind], data[(ind-1) / 2]);
        ind = (ind - 1) / 2;
    }
}

template<class T>
void Heap<T>::shift_down(size_t ind) {
    while (2*ind + 1 < size) {
        size_t li = 2*ind + 1;
        size_t ri = 2*ind + 2;
        size_t j = li;

        if (ri < size && data[ri] < data[li]) j = ri;
        if (data[ind] < data[j]) break;

        std::swap(data[ind], data[j]);
        ind = j;
    }
}

template<class T>
std::string Heap<T>::serialise_subtree(
    size_t idx,
    std::function<std::string (std::string, std::string, std::string)> build_value
) {
    std::string left = "", root = "", right = "";
    if (idx*2 + 1 < size) left = serialise_subtree(idx*2+1, build_value);
    if (idx*2 + 2 < size) right = serialise_subtree(idx*2+2, build_value);
    if (idx < size) root = std::to_string(data[idx]);
    
    return build_value(left, root, right);
}

////////////////////////////// HEAP PUBLIC IMPLS //////////////////////////////

template<class T>
typename Heap<T>::iterator Heap<T>::begin() const {
    return iterator(data, size, 0, order);
}

template<class T>
typename Heap<T>::iterator Heap<T>::end() const {
    return iterator(data, size, size, order);
}

template<class T>
typename Heap<T>::iterator Heap<T>::heap_subtree::begin() const {
    return iterator(
        parrent->data, parrent->size, idx, idx, parrent->order
    );
}

template<class T>
typename Heap<T>::iterator Heap<T>::heap_subtree::end() const {
    return parrent->end();
}

template<class T>
typename Heap<T>::iterator Heap<T>::find(typename Heap<T>::iterator from, typename Heap<T>::iterator to, T elem) const {
    return std::find(from, to, elem);
}

template<class T>
typename Heap<T>::iterator Heap<T>::find(T elem) const {
    return std::find(begin(), end(), elem);
}

template<class T>
typename Heap<T>::heap_subtree Heap<T>::subtree(T elem) {
    return heap_subtree(this, find(elem).idx);
}

template<class T>
typename Heap<T>::heap_subtree Heap<T>::subtree(typename Heap<T>::iterator elem_iter) {
    return heap_subtree(this, elem_iter.idx);
}

template<class T>
void Heap<T>::insert(T elem) {
    if (size+1 == memsize) resize();

    data[size] = elem;
    shift_up(size++);
}

template<class T>
void Heap<T>::remove(T elem) {
    T *el_ptr = std::find(data, data+size, elem);

    if (el_ptr == data+size) {
        throw std::runtime_error("Element not exists");
    }

    if (el_ptr == data+size-1) {
        --size;
        return;
    }

    *el_ptr = data[--size];
    if (elem < data[size]) shift_down(el_ptr-data);
    else shift_up(el_ptr-data);
}

template<class T>
std::string Heap<T>::serialize(std::string format) {
    return serialise_subtree(0, [&](std::string left, std::string root, std::string right) -> std::string {
        std::string new_str = format;

        if (auto it = new_str.find("{L}");  it != -1) new_str.replace(it, 3, left);
        if (auto it = new_str.find("{Ro}"); it != -1) new_str.replace(it, 4, root);
        if (auto it = new_str.find("{R}");  it != -1) new_str.replace(it, 3, right);

        return new_str;
    });
}

template<class T>
Heap<T> Heap<T>::get_subtree(T elem) {
    Heap res;
    for (auto &it: subtree(elem)) {
        res.insert(it);
    }
    return res;
}

template<class T>
Heap<T> Heap<T>::map(std::function<T (T)> f) {
    Heap res;
    for (auto &it: *this) {
        res.insert(f(it));
    }
    return res;
}

template<class T>
Heap<T> Heap<T>::where(std::function<bool (T)> f) {
    Heap res;
    for (auto &it: *this) {
        if (f(it)) res.insert(it);
    }
    return res;
}